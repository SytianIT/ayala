@extends('frontend.layouts.app')

@section('main-class', 'whiteform full-screen vertical-center')

@section('main-style', "background-image: url(" . asset('images/bgs/feedback.jpg') . ")")
@section('main')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="whiteform-form">
                    <div class="heading">
                        <h1 class="font-size-25 font-mont-bold text-center">Reset Password</h1>
                    </div>

                    <div class="content">

                        <form class="form form-horizontal floating-response" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-ayala btn-orange btn-wide ">
                                        Send Password Reset Link&nbsp;&nbsp;<i class="fa fa-send"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
