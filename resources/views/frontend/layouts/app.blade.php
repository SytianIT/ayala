<!DOCTYPE html>
<html lang="en">
<head>
    @yield('ng-app')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('/') }}">
    <meta name="full-url" content="{{ Request::fullUrl() }}">

    <title> {{ isset($title) ? 'Ayala Malls | ' . $title : 'Ayala Malls' }}</title>
    <meta name="description" content="{{ isset($metDesc) ? $metDesc : '' }}">
    <meta name="keywords" content="{{ isset($metKey) ? $metKey : '' }}">

    @yield('meta')

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favs/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favs/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favs/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favs/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favs/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favs/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favs/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favs/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favs/apple-icon-180x180.png') }}">

    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favs/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favs/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favs/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favs/favicon-16x16.png') }}">

    @yield('before-styles')

    <link rel="stylesheet" href="{{ asset('css/app.css') }}?version=1.4">
    @yield('styles')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    @yield('head-scripts')
</head>
<body>

    @if($isMobile)
        <nav id="mobile-sidebar" class="mobile-sidebar">
            <a href="javascript:;" class="toggle-mobile-menu pull-right font-size-30">
                <i class="fa fa-caret-square-o-left" aria-hidden="true"></i>
            </a>
            <ul class="list-unstyled clearfix">
                <li><a href="{{ route('user.dashboard') }}"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li><a href="{{ route('user.mission-vision') }}"><i class="fa fa-comments-o"></i>&nbsp;Mission & Vision</a></li>
                <li><a href="{{ route('user.survey') }}"><i class="fa fa-file-text-o"></i>&nbsp;Survey</a></li>
                <li>
                    <a href="{{ route('user.training') }}"><i class="fa fa-desktop"></i>&nbsp;Training <i class="mobile-sub-menu-toggle fa fa-chevron-down pull-right font-size-13 mrg5T"></i></a>
                    @if(count($trainingCat) > 0)
                        <ul class="sub-menu list-unstyled">
                            @foreach($trainingCat as $key => $trainCat)
                                <li><a href="{{ route('user.training', ['category' => $key]) }}">{!! $trainCat !!}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                <li>
                    <a href="{{ route('user.learning') }}"><i class="fa fa-mobile"></i>&nbsp;E-Learning <i class="mobile-sub-menu-toggle fa fa-chevron-down pull-right font-size-13 mrg5T"></i></a>
                    @if(count($learningCat) > 0)
                        <ul class="sub-menu list-unstyled">
                            @foreach($learningCat as $key => $learnCat)
                                <li><a href="{{ route('user.learning', ['category' => $key]) }}">{!! $learnCat !!}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                <li>
                    <a href="{{ route('user.job') }}"><i class="fa fa-briefcase"></i>&nbsp;Jobs <i class="mobile-sub-menu-toggle fa fa-chevron-down pull-right font-size-13 mrg5T"></i></a>
                    @if(count($departments) > 0)
                        <ul class="sub-menu list-unstyled">
                            @foreach($departments as $departmentCat)
                                <li><a href="{{ route('user.job.department', $departmentCat->slug) }}">{{ $departmentCat->title }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                <li>
                    <a href="{{ route('user.news') }}"><i class="fa fa-newspaper-o"></i>&nbsp;News & Events <i class="mobile-sub-menu-toggle fa fa-chevron-down pull-right font-size-13 mrg5T"></i></a>
                    @if(count($newsCat) > 0)
                        <ul class="sub-menu list-unstyled">
                            @foreach($newsCat as $key => $newsItem)
                                <li><a href="{{ route('user.news', ['category' => $key]) }}">{!! $newsItem !!}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                <li><a href="{{ route('user.profile') }}"><i class="fa fa-user-o"></i>&nbsp;My Account</a></li>
            </ul>
        </nav>
    @endif

    @if(!isset($noHeader))
        <header id="header" class="header">
            <div class="pane-logo">
                <a href="{{ route('user.dashboard') }}">
                    <img src="{{ asset('images/ayala-logo.png') }}" alt="{{ env('APP_NAMe') }}" class="img-responsive" style="height: 55px;">
                </a>
            </div>
            @guest
            <div class="guest-heading">
                <a href="{{ route('home') }}" class="font-size-16"><i class="fa fa-angle-left"></i> Back to Homepage</a>
            </div>
            @endguest
            @auth
            <div class="auth-heading">
                <div class="container">
                    <div class="inner">
                        <h3><a href="{{ route('user.dashboard') }}" style="color: #fff">HR Services</a></h3>
                        <div class="float-right">
                            <h3>Welcome {{ $authUser->username }}!</h3>
                            <a href="{{ route('logout') }}" class="btn btn-ayala btn-orange">Log out&nbsp;<i class="fa fa-sign-out" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                @include('frontend.partials.nav')
            </div>
            @endauth
        </header>
    @endif

    @auth
        <main id="main" class="main @yield('main-class')" style="@yield('main-style')">
            @yield('main')
        </main>
    @endauth

    @guest
        <main id="main" class="main @yield('main-class')" style="@yield('main-style')">
            @yield('main')
        </main>
    @endguest

    <footer id="footer" class="footer">
        <p class="text-center font-size-12">Copyright &copy; {{ \Date('Y') }} Ayalamalls 360 All Rights Reserved</p>
    </footer>
    <div class="my-loader">
        <img src="{{ asset('images/loader.gif') }}" alt="Ayala Malls 360 - Please wait" class="img-responsive preloader">
    </div>

    @if(isset($errors))
        @include('admin.partials.notifications')
    @endif

    @yield('modals')

    @yield('before-scripts')

    <script>
        var baseUrl = document.querySelectorAll("meta[name=base-url]")[0].getAttribute('content');
        var fullUrl = document.querySelectorAll("meta[name=full-url]")[0].getAttribute('content');
        var csrfToken = document.querySelectorAll("meta[name=csrf-token]")[0].getAttribute('content');
    </script>
    <script src="{{ asset('/js/app.js') }}?version=1.3"></script>

    @yield('scripts')

</body>
</html>