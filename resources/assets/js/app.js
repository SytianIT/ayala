$(document).ready(function () {

    $(window).scroll(function() {
        fadeFixedNav();
    });

    $(window).load(function () {
        $('.my-loader').addClass('my-hide');

        applyFullscreenHeight();

        fadeFixedNav();

        initEqualize();

        $(window).resize();

        if ($('.flexslider').length > 0) {
            $('.flexslider').flexslider({
                animation: "slide"
            });
        }
    });

    $(window).on('resize', function () {
        applyFullscreenHeight();
    })

    $(window).resize();

    // Toggle Mobile Menu
    $('.mobile-sub-menu-toggle').on('click', function (e) {
        e.preventDefault();
        var $this = $(this),
            anchor = $this.closest('a');

        if (anchor.hasClass('active')) {
            anchor.removeClass('active');
        } else {
            anchor.addClass('active');
        }
    });

    // Full Screen Section
    // Push Down Footer
    function applyFullscreenHeight() {
        var footerHeight = $('.footer').outerHeight(),
            headerHeight = $('.header').outerHeight(),
            contentHeight = $('.full-screen .container').outerHeight(),
            homeContentHeight = $('.homepage .homepage-content').outerHeight(),
            othersHeight  = (footerHeight != undefined ? footerHeight : 0) + (headerHeight != undefined ? headerHeight : 0) + (contentHeight != undefined ? contentHeight : 0),
            screenHeight  = $(window).outerHeight(),
            mainHeight = screenHeight - (footerHeight + headerHeight),
            paddingBoth = (screenHeight - othersHeight)/2,
            paddingTop = (screenHeight - othersHeight) - homeContentHeight;

        $('.full-screen.vertical-center').not('.homepage').css({
            'paddingTop' : paddingBoth,
            'paddingBottom' : paddingBoth
        });

        $('.full-screen').not('.vertical-center').css({
            'min-height' : mainHeight
        });
    }

    /* Initialize BTN UPLOADER */
    $('.file-uploader').on('click', function(){
        var inputEle = $(this).closest('.uploader').find('.file-input');
        inputEle.trigger('click');
    });

    function readURL(input) {
        var targetDisplay = $(input).data('target');

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(targetDisplay).val(input.files[0].name)
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.file-input').on('change', function(){
        readURL(this);
    });

    $('.toggle-mobile-menu').on('click', function(){
        $('#mobile-sidebar').toggleClass('show');
    })

    /* Initialize form auto submit when filter changed */
    if ($('.form-filter').length > 0){
        var formFilter = $('.form-filter'),
            inpFilter = $('.input-filter');

        inpFilter.on('change', function () {
            var $this = $(this);
            if ($(this).hasClass('select-department')) {
                var url = $this.find('option:selected').attr('data-url');
                location.href = url;
            } else {
                formFilter.submit();
            }
        })
    }

    /* jGrowl Alert */
    var $stickyAlert = $(".hidden-alert");
    if ($stickyAlert.length) {
        var $alertNotifType = $stickyAlert.data('notif'),
            $alertText = $stickyAlert.html();

        if( $alertNotifType == "error" ) {
            var $theme = 'bg-red';
        }
        else if( $alertNotifType == "danger" ) {
            var $theme = 'bg-orange';
        }
        else {
            $theme = 'bg-green';
        }

        $.jGrowl( $alertText, {
            sticky: false,
            position: 'top-right',
            theme: $theme,
            life : 3000,
        });
    }

    function fadeFixedNav(){
        var prh = $('.pane-logo').outerHeight(),
            urh = $('.auth-heading .container').outerHeight();

        if(Modernizr.mq('(max-width: 768px)')) {
            urh = prh + urh;
        }

        if($(document).scrollTop() > urh){
            $('.main-menu').addClass('navbar-fixed-top').css('position','fixed');
        } else {
            $('.main-menu').removeClass('navbar-fixed-top').css('position','absolute');
        }
    }

    function initEqualize(){
        // EQUALIZE
        if(Modernizr.mq('(min-width: 768px)')) {
            var maxHeight = 0;
            $('.equalize').each(function() {
                maxHeight = $(this).outerHeight() > maxHeight ? $(this).outerHeight() : maxHeight;
            }).css('height', maxHeight + 'px');
        } else {
            $('.equalize').css('height', 'auto');
        }
    }
});