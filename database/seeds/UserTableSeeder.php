<?php

use App\User;
use Ayala\Role\Role;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\Table;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        User::truncate();
        DB::table('user_roles')->truncate();
        SiteSettings::truncate();

        $suRole = new Role();
        $suRole->key = Role::SU;
        $suRole->title = 'Super Admin';
        $suRole->save();

        $adminRole = new Role();
        $adminRole->key = Role::ADMIN;
        $adminRole->title = 'Admin';
        $adminRole->save();

        $userRole = new Role();
        $userRole->key = Role::USER;
        $userRole->title = 'User';
        $userRole->save();

        $su = new User();
        $su->first_name = 'Super Admin';
        $su->username = 'admin';
        $su->email = 'admin@email.com';
        $su->password = bcrypt('admin123');
        $su->save();
        $su->roles()->attach($suRole);

        $suSytian = new User();
        $suSytian->first_name = 'Kenneth';
        $suSytian->username = 'kenneth';
        $suSytian->email = 'kenneth@email.com';
        $suSytian->password = bcrypt('admin123');
        $suSytian->save();
        $suSytian->roles()->attach($suRole);

        $settings = new SiteSettings();
        $settings->save();

        /*$suSytian = new User();
        $suSytian->first_name = 'User';
        $suSytian->username = 'user';
        $suSytian->email = 'user@email.com';
        $suSytian->password = bcrypt('user123');
        $suSytian->save();
        $suSytian->roles()->attach($userRole);*/
    }
}
