<?php

namespace App\Console\Commands;

use Ayala\Survey\Survey;
use Ayala\Survey\SurveyRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class ExpiringSurvey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:expiring_survey';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check and update any expiring survey';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        SurveyRepository $surveyRepo
    )
    {
        parent::__construct();
        $this->surveyRepo = $surveyRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request)
    {
        $request['active'] = true;
        $survey = $this->surveyRepo->search($request);

        foreach ($survey as $item) {
            $today = Carbon::today();

            /*if ($survey->date_from->lt($today)) {
                $survey->status = Survey::STATS_ACTIVE;
            }*/

            if ($item->date_to->lt($today)) {
                $item->status = Survey::STATS_EXPIRED;
            }

            $item->save();
        }
    }
}
