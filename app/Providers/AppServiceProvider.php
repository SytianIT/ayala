<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Agent\Agent;
use Laravel\Passport\Passport;

use Ayala\Learning\Category as LearningCategory;
use Ayala\Training\Category as TrainingCategory;
use Ayala\News\Category as NewsCategory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('*', function($view) {
            $view->with('authUser', Auth::user());
        });

        view()->composer('frontend.*', function($view) {
            $agent = new Agent();
            $isMobile = $agent->isMobile() ? true : false;

            $view->with('isMobile', $isMobile);
        });

        view()->composer('frontend.layouts.app', function($view) {
            $departmentRepo = resolve('Ayala\Jobs\DepartmentRepository');

            $learningCat = LearningCategory::getNestedList('title', 'id', '-- ');
            $trainingCat = TrainingCategory::getNestedList('title', 'id', '-- ');
            $departments = $departments = $departmentRepo->search(new Request());
            $newsCat = NewsCategory::getNestedList('title', 'id', '-- ');

            $view->with('learningCat', $learningCat);
            $view->with('trainingCat', $trainingCat);
            $view->with('departments', $departments);
            $view->with('newsCat', $newsCat);
        });

        Blade::directive('dateFormat', function($expression) {
            return "<?php echo with($expression)->format('d-M-y'); ?>";
        });

        Blade::directive('dateTimeFormat', function($expression) {
            return "<?php echo with($expression)->format('d-M-y h:i A'); ?>";
        });

        Blade::directive('timeFormat', function($expression) {
            return "<?php echo with($expression)->format('h:i A'); ?>";
        });

        Blade::directive('dateDayFormat', function($expression) {
            return "<?php echo with(new Carbon\Carbon($expression))->toFormattedDateString() . ' ' . with(new Carbon\Carbon($expression))->format('l'); ?>";
        });

        // Create passport auth tokens
        Passport::routes();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
