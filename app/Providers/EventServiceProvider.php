<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewJobApplication' => [
            'App\Listeners\NewJobApplicationListener',
        ],
        'App\Events\Feedback' => [
            'App\Listeners\FeedbackListener',
        ],
        'App\Events\Registration' => [
            'App\Listeners\RegistrationListener',
        ],
        'App\Events\NewSurvey' => [
            'App\Listeners\NewSurveyListener',
        ],
        'App\Events\NewTraining' => [
            'App\Listeners\NewTrainingListener',
        ],
        'App\Events\NewLearning' => [
            'App\Listeners\NewLearningListener',
        ],
        'App\Events\NewEventsAndNews' => [
            'App\Listeners\NewEventsAndNewsListener',
        ],
        'App\Events\NewJob' => [
            'App\Listeners\NewJobListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
