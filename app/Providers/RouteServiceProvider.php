<?php

namespace App\Providers;

use Ayala\Jobs\Job;
use Ayala\Registration\Registration;
use Ayala\Survey\Survey;
use Ayala\Training\Training;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    protected $apiNamespace = 'App\Http\Controllers\Api';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        // Enable find of model if trashed
        Route::bind('survey', function ($value) {
            return Survey::withTrashed()->find($value);
        });

        Route::bind('training', function ($value) {
            return Training::withTrashed()->find($value);
        });

        Route::bind('job', function ($value) {
            return Job::withTrashed()->find($value);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('web') //->middleware('api') for real api
             ->namespace($this->apiNamespace)
             ->group(base_path('routes/api.php'));
    }
}
