<?php

namespace App;

use Ayala\Employee\Employee;
use Ayala\Role\Role;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Watson\Validating\ValidatingTrait;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;
    use ValidatingTrait;

    //protected $username = 'username';

    protected $hiddenUser = 'kenneth';

    protected $rules = [ ];

    protected $casts = [
        'active' => 'boolean'
    ];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    /**
     * Scope Queries
     */
    public function scopeAdmins($query)
    {
        return $query->whereHas('roles', function ($q) {
            $q->whereIn('key', [Role::ADMIN, Role::SU]);
        })->where('id', '<>', Auth::user()->id)->where('id', '<>', 1);
    }

    public function scopeEmployees($query)
    {
        return $query->whereHas('roles', function ($q) {
            $q->where('key', Role::USER);
        });
    }

    /**
     * Getter and Setters
     */
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Helper Methods
     */
    public function isSuperAdmin()
    {
        if ($this->roles->where('key', Role::SU)->first()) {
            return true;
        }

        return false;
    }

    public function isAdmin()
    {
        if ($this->roles->whereIn('key', [Role::SU, Role::ADMIN])->first()) {
            return true;
        }

        return false;
    }

    public function isUser()
    {
        if ($this->roles->where('key', Role::USER)->first()) {
            return true;
        }

        return false;
    }

    /*
     * Helper for Audits
     */
    public function audit($type)
    {
        $auditRepo = resolve(\Ayala\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New admin '{$this->username}'";
    }

    public function auditUpdate()
    {
        return "Admin '{$this->username}'";
    }

    public function auditDelete()
    {
        return "Admin '{$this->username}'";
    }
}
