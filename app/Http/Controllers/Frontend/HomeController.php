<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Ayala\Feedback\FeedbackRepository;
use Ayala\Jobs\DepartmentRepository;
use Ayala\SiteSettings\SiteSettings;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        DepartmentRepository $departmentRepo,
        FeedbackRepository $feedbackRepo
    )
    {
        $this->departmentRepo = $departmentRepo;
        $this->feedbackRepo = $feedbackRepo;
        $this->settings = SiteSettings::getSettings();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Home";

        $sliders = $this->settings->uploads()->orderBy('order')->get();
        $settings = SiteSettings::getSettings();

        return view('frontend.home', compact('sliders', 'title', 'settings'));
    }

    public function feedback(Request $request)
    {
        $title = "Feedback";

        $departments = $this->departmentRepo->search($request);

        return view('frontend.feedback.feedback', compact('departments','title'));
    }

    public function feedbackStore(Request $request)
    {
        try {
            $feedback = $this->feedbackRepo->createFeedback($request);

            return response()->json(['success' => true, 'message' => 'feedback sent!', 'redirect' => '/feedback/thank-you'], 200);
        } catch (ValidationException $e) {
            return response()->json(['success' => false, 'errors' => $e->getErrors()], 200);
        }

    }

    public function feedbackThankyou()
    {
        $title = "Thank You";

        return view('frontend.feedback.thank-you', compact('title'));
    }
}
