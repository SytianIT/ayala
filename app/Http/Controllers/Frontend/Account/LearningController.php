<?php

namespace App\Http\Controllers\Frontend\Account;

use Ayala\Learning\Category;
use Ayala\Learning\LearningRepository;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LearningController extends Controller
{
    public function __construct(
        LearningRepository $learningRepo
    )
    {
        $this->learningRepo = $learningRepo;
    }

    public function index(Request $request)
    {
        $title = "e-Learnings";

        $request['active'] = true;
        $request['per_page'] = 6;
        $learnings = $this->learningRepo->search($request);
        $categories = Category::getNestedList('title', 'id', ' -- ');
        $settings = SiteSettings::getSettings();

        return view('frontend.account.learning.index', compact('learnings','categories','settings','title'));
    }

    public function show($slug)
    {
        $learning = $this->learningRepo->getThisSlug($slug)->load('related');
        $title = "e-Learnings - " . $learning->title;

        return view('frontend.account.learning.show', compact('learning','title'));
    }
}
