<?php

namespace App\Http\Controllers\Frontend\Account;

use Ayala\Employee\Employee;
use Ayala\SiteSettings\SiteSettings;
use Ayala\Survey\Survey;
use Ayala\Survey\SurveyItems;
use Ayala\Survey\SurveyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveyController extends Controller
{
    public function __construct(
        SurveyRepository $surveyRepo
    )
    {
        $this->surveyRepo = $surveyRepo;
    }

    public function index(Request $request)
    {
        $title = "Survey";

        $user = $request->user();

        $employee = $user->employee;
        $userSurveys = $user->employee->surveys;
        $surveys = $this->surveyRepo->getThisApplicableSurveys($request, ['employees']);
        $settings = SiteSettings::getSettings();

        return view('frontend.account.survey.index', compact('surveys', 'userSurveys', 'employee', 'settings', 'title'));
    }

    public function show(Request $request, $slug)
    {
        $survey = $this->surveyRepo->getThisSlug($slug);

        if ($survey) {
            $title = "Survey - " . $survey->title;

            $user = $request->user();

            if ($user->employee) {
                $existing = $user->employee->surveys()->find($survey->id);

                if ($existing && in_array($existing->pivot->status, [Survey::STATS_COMPLETED, Survey::STATS_PROCESSED])) {
                    $statistics = $this->surveyRepo->getStatistics($survey);
                    $survey->load([
                        'items' => function($q) {
                            $q->whereIn('type',[SurveyItems::TYPE_CHECKBOX, SurveyItems::TYPE_RADIO])
                              ->where('is_restricted', false);
                        }
                    ]);
                    return view('frontend.account.survey.completed', compact('survey', 'employee', 'statistics', 'title'));
                }

                return view('frontend.account.survey.show', compact('survey', 'employee', 'title'));
            }
        }

        return redirect()->back()
                         ->withError('Something is wrong with your account, please try again access later.');
    }

    public function thankYou($slug)
    {
        $title = 'Thank You';
        $survey = $this->surveyRepo->getThisSlug($slug);

        return view('frontend.account.survey.thank-you', compact('survey', 'title'));
    }
}
