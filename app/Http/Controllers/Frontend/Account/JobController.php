<?php

namespace App\Http\Controllers\Frontend\Account;

use Ayala\Jobs\Department;
use Ayala\Jobs\DepartmentRepository;
use Ayala\Jobs\JobRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    public function __construct(
        JobRepository $jobRepo,
        DepartmentRepository $departmentRepo
    )
    {
        $this->jobRepo = $jobRepo;
        $this->departmentRepo = $departmentRepo;
    }

    public function index(Request $request)
    {
        $title = "Departments";

        $departments = $this->departmentRepo->search($request);

        return view('frontend.account.job.index', compact('departments', 'title'));
    }

    public function showDepartment(Request $request, $slug)
    {
        $department = $this->departmentRepo->getThisSlug($slug);

        if ($department == null) {
            abort(404);
        }

        $title = "Department - " . $department->title;

        $request['department'] = $department->id;
        $request['active'] = true;
        $request['per_page'] = 20;
        $jobs = $this->jobRepo->search($request)->sortBy('title');
        $departments = $this->departmentRepo->search($request);

        return view('frontend.account.job.department', compact('jobs','departments', 'department', 'title'));
    }

    public function show($slug)
    {
        $job = $this->jobRepo->getThisSlug($slug)->load('department');
        $title = "Job - " . $job->title;

        return view('frontend.account.job.show', compact('job', 'title'));
    }

    public function register(Request $request, $slug)
    {
        try {
            $this->jobRepo->register($request, $slug);

            return response()->json(['success' => true, 'message' => 'registration sent!', 'redirect' => '/register/thank-you'], 200);
        } catch (ValidationException $e) {
            return response()->json(['success' => false, 'errors' => $e->getErrors()], 200);
        }
    }

    public function thankYou($slug)
    {
        $title = "Thank You";

        $job = $this->jobRepo->getThisSlug($slug);

        return view('frontend.account.job.thank-you', compact('job', 'title'));
    }
}
