<?php

namespace App\Http\Controllers\Frontend\Account;

use Ayala\News\Category;
use Ayala\News\NewsRepository;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function __construct(
        NewsRepository $newsRepo
    )
    {
        $this->newsRepo = $newsRepo;
    }

    public function index(Request $request)
    {
        $title = "News & Events";

        $request['per_page'] = 6;
        $request['active'] = true;
        $news = $this->newsRepo->search($request);
        $categories = Category::getNestedList('title', 'id', ' -- ');
        $settings = SiteSettings::getSettings();

        return view('frontend.account.news.index', compact('news','categories','settings','title'));
    }

    public function show($slug)
    {
        $news = $this->newsRepo->getThisSlug($slug);
        $title = "News & Events - " . $news->title;

        return view('frontend.account.news.show', compact('news', 'title'));
    }
}
