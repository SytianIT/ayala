<?php

namespace App\Http\Controllers\Frontend\Account;

use Ayala\SiteSettings\SiteSettings;
use Ayala\Training\Category;
use Ayala\Training\CategoryRepository;
use Ayala\Training\Training;
use Ayala\Training\TrainingRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainingController extends Controller
{
    public function __construct(
        TrainingRepository $trainingRepo
        //CategoryRepository $categoryRepo
    )
    {
        $this->trainingRepo = $trainingRepo;
        //$this->categoryRepo = $categoryRepo;
    }

    public function index(Request $request)
    {
        $title = "Trainings";

        $request['active'] = true;
        $request['per_page'] = 6;
        $trainings = $this->trainingRepo->search($request);
        $categories = Category::getNestedList('title', 'id', ' -- ');
        $settings = SiteSettings::getSettings();

        return view('frontend.account.training.index', compact('trainings','categories','settings','title'));
    }

    public function show($slug)
    {
        $training = $this->trainingRepo->getThisSlug($slug);
        $title = "Training - " . $training->title;

        return view('frontend.account.training.show', compact('training','title'));
    }

    public function register(Request $request, $slug)
    {
        try {
            $this->trainingRepo->register($request, $slug);

            return response()->json(['success' => true, 'message' => 'registration sent!', 'redirect' => '/register/thank-you'], 200);
        } catch (ValidationException $e) {
            return response()->json(['success' => false, 'errors' => $e->getErrors()], 200);
        }
    }

    public function thankYou($slug)
    {
        $title = "Thank You";

        $training = $this->trainingRepo->getThisSlug($slug);

        return view('frontend.account.training.thank-you', compact('training','title'));
    }

    public function cancel(Request $request, $slug)
    {
        $training = $this->trainingRepo->getThisSlug($slug);

        $user = $request->user();
        $registration = $training->registrations()->where('employee_id', $user->employee->id)->first();
        $registration->delete();

        return redirect()->back()->withSuccess('Registration Cancelled!');
    }
}
