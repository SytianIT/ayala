<?php

namespace App\Http\Controllers\Frontend\Account;

use Ayala\Jobs\Job;
use Ayala\Jobs\JobRepository;
use Ayala\Learning\Learning;
use Ayala\Learning\LearningRepository;
use Ayala\News\News;
use Ayala\News\NewsRepository;
use Ayala\SiteSettings\SiteSettings;
use Ayala\Survey\SurveyRepository;
use Ayala\Training\Training;
use Ayala\Training\TrainingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct(
        JobRepository $jobRepo,
        LearningRepository $learningRepo,
        NewsRepository $newsRepo,
        SurveyRepository $surveyRepo,
        TrainingRepository $trainingRepo
    )
    {
        $this->jobRepo = $jobRepo;
        $this->learningRepo = $learningRepo;
        $this->newsRepo = $newsRepo;
        $this->surveyRepo = $surveyRepo;
        $this->trainingRepo = $trainingRepo;
    }

    public function index(Request $request)
    {
        $title = 'Account Dashboard';

        $user = $request->user();

        if ($user->isAdmin()) {
            return redirect()->route('admin.dashboard');
        }

        $employee = $user->employee;

        $survey = $this->surveyRepo->getThisApplicableSurveys($request)->first();
        $request['active'] = true;
        $training = $this->trainingRepo->search($request)->first(); //Training::latest()->first();
        $news = $this->newsRepo->search($request)->first(); //News::latest()->first();
        $job = $this->jobRepo->search($request)->first(); //Job::latest()->first();
        $learning = $this->learningRepo->search($request)->first(); //Learning::latest()->first();

        return view('frontend.account.index', compact('title', 'survey', 'training', 'news', 'job', 'learning', 'employee'));
    }

    public function missionVision()
    {
        $title = 'Ayala Malls Mission & Vision';

        $settings = SiteSettings::getSettings();

        return view('frontend.account.others.mission-vision', compact('title', 'settings'));
    }

    public function survey()
    {
        return view('frontend.account.survey.index');
    }
}
