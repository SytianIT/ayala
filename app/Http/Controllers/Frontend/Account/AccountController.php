<?php

namespace App\Http\Controllers\Frontend\Account;

use Ayala\User\UserRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function __construct(
        Auth $user,
        UserRepository $userRepo
    )
    {
        $this->user = $user;
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        $title = "Account";

        $user = $this->user;

        return view('frontend.account.account.index', compact('user', 'title'));
    }

    public function update(Request $request)
    {
        try {
            $user = $request->user();
            $user = $this->userRepo->updatePassword($request, $user);

            return redirect()->route('user.profile')
                ->withSuccess('Password updated!');
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
