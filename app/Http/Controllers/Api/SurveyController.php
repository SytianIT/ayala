<?php

namespace App\Http\Controllers\Api;

use Ayala\Survey\Survey;
use Ayala\Survey\SurveyRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;

class SurveyController extends ApiController
{
    public function __construct(
        SurveyRepository $surveyRepo
    )
    {
        $this->surveyRepo = $surveyRepo;
    }

    public function get(Request $request, Survey $survey)
    {
        $user = $request->user();
        $employee = $user->employee;

        $survey->load(['items', 'items.answers']);
        $survey->items = $survey->items->filter(function($q) use ($employee) {
            return $q->answers->filter(function ($q) use ($employee) {
                return $q->employee_id == $employee->id;
            });
        });

        return $survey;
    }

    public function save(Request $request)
    {
        try {
            $response = $this->surveyRepo->saveSurveyAnswers($request);

            return response()->json($response);
        } catch (ValidationException $e) {
            return $this->errorResponse($e->getErrors());
        }
    }

    public function submit(Request $request)
    {
        try {
            $response = $this->surveyRepo->submitSurveyAnswers($request);

            return response()->json($response);
        } catch (ValidationException $e) {
            return $this->errorResponse($e->getErrors());
        }
    }
}
