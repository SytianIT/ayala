<?php

namespace App\Http\Controllers\Api;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

abstract class ApiController extends Controller
{
    protected function errorResponse($errors = [])
    {
        if ($errors instanceof Arrayable) {
            $errors = $errors->toArray();
        }
        $messages = is_array($errors) ? $errors : [$errors];

        return response()->json(array('messages' => $messages), 400);
    }

    protected function successResponse($msg)
    {
        return response()->json(array('message' => $msg));
    }

    protected function emptyResponse()
    {
        return response()->json(array());
    }
}
