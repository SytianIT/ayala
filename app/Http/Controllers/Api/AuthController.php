<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends ApiController
{
    public function getAuth(Request $request)
    {
        return $request->user();
    }

    public function getEmployee(Request $request)
    {
        if ($request->user()->employee) {
            return $request->user()->employee->load([
                'surveys' => function($q) use($request){
                    $q->where('survey_id', $request->survey_id);
                },
                'answers' => function($q) use($request){
                    $q->whereIn('survey_item_id', $request->ids);
                }
            ]);
        }

        return $this->errorResponse(['Unauthorized.']);
    }
}
