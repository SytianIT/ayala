<?php

namespace App\Http\Controllers\Admin;

use Ayala\Registration\Registration;
use Ayala\Registration\RegistrationRepository;
use Ayala\Training\Training;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainingRegistrationController extends Controller
{
    public function __construct(
        RegistrationRepository $registrationRepo
    )
    {
        $this->registrationRepo = $registrationRepo;
    }

    public function index(Request $request)
    {
        $title = 'Training Registrations';
        $isTrashed = false;

        $training = Training::withTrashed()->find($request->training);
        if ($training && $training->trashed()) {
            $isTrashed = true;
            $request['trashed'] = true;
        }

        $request['trainings'] = true;
        $registrations = $this->registrationRepo->search($request, ['relatable']);
        if (!$isTrashed) {
            $trainingsCollection = Training::latest()->pluck('title', 'id');
        }

        $query = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_data,
            'training' => $request->training,
            'position' => $request->position,
            'job_status' => $request->status,
            'per_page' => $request->per_page ?: self::PER_PAGE
        ];

        return view('admin.training.registration.index', compact('trainingsCollection', 'registrations', 'query', 'title', 'isTrashed', 'training'));
    }

    public function show(Registration $registration)
    {
        $title = 'View Registration';
        $registration->load([
            'relatable' => function($q){
                 $q->withTrashed();
            }
        ]);
        $training = $registration->relatable;

       return view('admin.training.registration.show', compact('registration', 'title', 'training'));
    }

    public function remove(Registration $registration)
    {
        $registration->load(['relatable' => function($q) { $q->withTrashed(); }]);
        $registration->audit('delete');
        $registration->forceDelete();

        return redirect()->back()
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Registration "]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->registrationRepo->doBulkRemove($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('Registrations deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    public function update(Request $request, Registration $registration)
    {
        $registration->status = $request->status;
        $registration->save();
        $registration->audit('update');

        return redirect()->route('admin.trainings.registrations.view', $registration)
            ->withSuccess(__('validation.request.update', ['prefix' => "Registration status "]));
    }
}
