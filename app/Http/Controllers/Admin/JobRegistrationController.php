<?php

namespace App\Http\Controllers\Admin;

use Ayala\Jobs\Job;
use Ayala\Registration\Registration;
use Ayala\Registration\RegistrationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobRegistrationController extends Controller
{
    public function __construct(
        RegistrationRepository $registrationRepo
    )
    {
        $this->registrationRepo = $registrationRepo;
    }

    public function index(Request $request)
    {
        $title = 'Job Applications';
        $isTrashed = false;

        $job = Job::withTrashed()->find($request->job);
        if ($job && $job->trashed()) {
            $isTrashed = true;
            $request['trashed'] = true;
        }

        $request['jobs'] = true;
        $registrations = $this->registrationRepo->search($request, ['relatable']);
        if (!$isTrashed) {
            $jobsCollection = Job::latest()->plucK('title', 'id');
        }
        //$departments = Department::latest()->plucK('title', 'id');

        $query = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_data,
            'job' => $request->job,
            'job_status' => $request->status,
            'department' => $request->department,
            'name' => $request->name,
            'referrals' => $request->referrals,
            'per_page' => $request->per_page ?: self::PER_PAGE
        ];

        return view('admin.job.registration.index', compact('jobsCollection', 'registrations', 'query', 'title', 'isTrashed', 'job'));
    }

    public function show(Registration $registration)
    {
        $title = 'View Registration';
        $registration->load([
            'relatable' => function($q){
                $q->withTrashed();
            }
        ]);
        $job = $registration->relatable;

        return view('admin.job.registration.show', compact('registration', 'title', 'job'));
    }

    public function remove(Registration $registration)
    {
        $registration->load(['relatable' => function($q) { $q->withTrashed(); }]);
        $registration->audit('delete');
        $registration->delete();

        return redirect()->back()
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Application "]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->registrationRepo->doBulkRemove($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('Applications deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    public function update(Request $request, Registration $registration)
    {
        $registration->status = $request->status;
        $registration->save();
        $registration->audit('update');

        return redirect()->route('admin.jobs.registrations.view', $registration)
            ->withSuccess(__('validation.request.update', ['prefix' => "Application status "]));
    }
}
