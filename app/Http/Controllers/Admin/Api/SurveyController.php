<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Api\ApiController;
use Ayala\Survey\SurveyRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;

class SurveyController extends ApiController
{
    public function __construct(
        SurveyRepository $surveyRepo
    )
    {
        $this->surveyRepo = $surveyRepo;
    }

    public function store(Request $request)
    {
        try {
            $survey = $this->surveyRepo->addSurvey($request);
            $survey->load(['uploads','malls','roles','employees']);

            return response()->json($survey->toArray());
        } catch (ValidationException $e) {
            return $this->errorResponse($e->getErrors());
        }
    }

    public function saveItems(Request $request)
    {
        try {
            $survey = $this->surveyRepo->storeSurveyItems($request);
            $survey->load(['uploads','malls','roles','items','employees']);

            return response()->json([
                'success' => true,
                'survey' => $survey->toArray(),
                'redirect_url' => route('admin.survey'),
                'message' => __('validation.request.create', ['prefix' => "Survey "])
            ]);
        } catch (ValidationException $e) {
            return $this->errorResponse($e->getErrors());
        }
    }
}
