<?php

namespace App\Http\Controllers\Admin\Api;

use Ayala\Uploadable\UploadableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EditorController extends Controller
{
    public function __construct(
        UploadableRepository $uploadRepo
    )
    {
        $this->uploadRepo = $uploadRepo;
    }

    public function imageUpload(Request $request)
    {
        $file = $request->file;
        $path = $this->uploadRepo->uploadImageIntervention($file, $request, ['path' => 'uploads/content'], false);

        return response()->json(['success' => 'success', 'location' => $path['path']]);
    }
}
