<?php

namespace App\Http\Controllers\Admin;

use Ayala\Icons\IconRepository;
use Ayala\Jobs\DepartmentRepository;
use Ayala\Jobs\Job;
use Ayala\Jobs\JobRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    public function __construct(
        JobRepository $jobRepo,
        DepartmentRepository $departmentRepo,
        IconRepository $iconRepo
    )
    {
        $this->jobRepo = $jobRepo;
        $this->departmentRepo = $departmentRepo;
        $this->iconRepo = $iconRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Jobs';

        $jobs = $this->jobRepo->search($request, ['department']);
        $trashedCount = $this->jobRepo->getTrashed()->count();

        $query = [
            'search' => $request->search,
            'per_page' => $request->per_page
        ];

        return view('admin.job.index', compact('title', 'jobs', 'trashedCount', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create Job';

        $departments = $this->departmentRepo->search($request);
        $icons = $this->iconRepo->search($request)->sortBy('title');

        return view('admin.job.create', compact('title', 'departments', 'icons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $job = $this->jobRepo->addJobs($request);

            return redirect()->route('admin.jobs')
                ->withSuccess(__('validation.request.create', ['prefix' => "Jobs '$job->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Job $job)
    {
        $title = 'Edit Job';

        $departments = $this->departmentRepo->search($request);
        $icons = $this->iconRepo->search($request)->sortBy('title');

        return view('admin.job.edit', compact('title', 'departments', 'job', 'icons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        try {
            $job = $this->jobRepo->updateJobs($request, $job);

            return redirect()->route('admin.jobs')
                ->withSuccess(__('validation.request.update', ['prefix' => "Job '$job->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        // To create logs here
        $title = $job->title;
        $this->jobRepo->deleteRegistrations($job);

        $job->audit('delete');
        $job->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Job '$title'"]));
    }

    /**
     * Display a listing of the resource (trashed).
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $title = 'Trashed Jobs';

        $jobsCount = $this->jobRepo->search($request);

        $request['trashed'] = true;
        $jobs = $this->jobRepo->search($request);

        return view('admin.job.trashed', compact('jobs','title', 'jobsCount'));
    }

    public function recover($job)
    {
        $job->restore();
        $job->audit('recover');
        $job->department()->withTrashed()->restore();
        $this->jobRepo->restoreRegistrations($job);

        return redirect()->route('admin.jobs')
            ->withSuccess(__('validation.request.restore', ['prefix' => "Job "]));
    }

    public function destroyPermanent($job)
    {
        $title = $job->title;
        $this->jobRepo->deletePermanent($job);

        return redirect()->route('admin.jobs.trashed')
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Job '$title'"]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->jobRepo->doJobBulkRemove($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('Items deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    public function import()
    {
        return view('admin.job.import');
    }

    public function load(Request $request)
    {
        try {
            $data = $this->jobRepo->loadExcel($request, true);

            return view('admin.job.inc.excel-prepared', compact('data'));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e, true);
        }
    }

    public function doImport(Request $request)
    {
        try {
            $data = $this->jobRepo->loadExcel($request);

            return redirect()->route('admin.jobs')
                ->withSuccess('Import success!');
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
