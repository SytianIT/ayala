<?php

namespace App\Http\Controllers\Admin;

use Ayala\Learning\Category;
use Ayala\Learning\CategoryRepository;
use Ayala\Learning\Learning;
use Ayala\Learning\LearningRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LearningController extends Controller
{
    public function __construct(
        LearningRepository $learningRepo,
        CategoryRepository $categoryRepo
    )
    {
        $this->learningRepo = $learningRepo;
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Learnings';
        $learnings = $this->learningRepo->search($request, ['categories']);
        $trashedCount = $this->learningRepo->getTrashed()->count();

        $query = [
            'search' => $request->search,
            'per_page' => $request->per_page
        ];

        return view('admin.learning.index', compact('title', 'learnings', 'trashedCount', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create Learning';

        $categories = Category::getNestedList('title', 'id', ' -- ');
        $learnings = $this->learningRepo->search($request, ['categories','related']);

        return view('admin.learning.create', compact('title', 'categories', 'learnings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $learning = $this->learningRepo->addLearning($request);

            return redirect()->route('admin.learnings')
                ->withSuccess(__('validation.request.create', ['prefix' => "Learning '$learning->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Learning $learning)
    {
        $title = 'Edit Learning';

        $categories = Category::getNestedList('title', 'id', ' -- ');
        $request['except_id'] = $learning->id;
        $learnings = $this->learningRepo->search($request, ['categories', 'related']);

        return view('admin.learning.edit', compact('title', 'categories', 'learning', 'learnings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Learning $learning)
    {
        try {
            $learning = $this->learningRepo->updateLearning($request, $learning);

            return redirect()->route('admin.learnings')
                ->withSuccess(__('validation.request.update', ['prefix' => "Learning '$learning->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Learning $learning)
    {
        // To create logs here
        $title = $learning->title;
        $learning->audit('delete');
        $learning->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Learning '$title'"]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->learningRepo->doBulkRemove($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('e-Learnings deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    /**
     * Display a listing of the resource (trashed).
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $title = 'Trashed Learnings';

        $learningsCount = $this->learningRepo->search($request);

        $request['trashed'] = true;
        $learnings = $this->learningRepo->search($request);

        return view('admin.learning.trashed', compact('learnings','title', 'learningsCount'));
    }

    public function recover($learning)
    {
        $learning = Learning::withTrashed()->where('id', $learning)->restore();

        return redirect()->route('admin.learnings')
            ->withSuccess(__('validation.request.restore', ['prefix' => "Learning "]));
    }

    public function destroyPermanent($learning)
    {
        $learning = Learning::withTrashed()->where('id', $learning)->first();
        $title = $learning->title;
        $learning->forcedelete();

        return redirect()->route('admin.learnings.trashed')
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Learning '$title'"]));
    }
}
