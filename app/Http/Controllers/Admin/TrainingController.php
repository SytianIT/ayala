<?php

namespace App\Http\Controllers\Admin;

use Ayala\Training\Category;
use Ayala\Training\CategoryRepository;
use Ayala\Training\Training;
use Ayala\Training\TrainingRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainingController extends Controller
{
    public function __construct(
        TrainingRepository $trainingRepo,
        CategoryRepository $categoryRepo
    )
    {
        $this->trainingRepo = $trainingRepo;
        $this->categoryRepo = $categoryRepo;
    }
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Trainings';
        $trainings = $this->trainingRepo->search($request, ['categories']);
        $trashedCount = $this->trainingRepo->getTrashed()->count();

        $query = [
            'search' => $request->search,
            'per_page' => $request->per_page
        ];

        return view('admin.training.index', compact('title', 'trainings', 'trashedCount', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create Training';

        $categories = Category::getNestedList('title', 'id', ' -- ');

        return view('admin.training.create', compact('title', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $training = $this->trainingRepo->addTraining($request);

            return redirect()->route('admin.trainings')
                ->withSuccess(__('validation.request.create', ['prefix' => "Training '$training->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Training $training)
    {
        $title = 'Edit Training';

        $categories = Category::getNestedList('title', 'id', ' -- ');

        return view('admin.training.edit', compact('title', 'categories', 'training'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Training $training)
    {
        try {
            $training = $this->trainingRepo->updateTraining($request, $training);

            return redirect()->route('admin.trainings')
                ->withSuccess(__('validation.request.update', ['prefix' => "Training '$training->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Training $training)
    {
        // To create logs here
        $title = $training->title;
        $this->trainingRepo->deleteRegistrations($training);

        $training->audit('delete');
        $training->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Training '$title'"]));
    }

    /**
     * Display a listing of the resource (trashed).
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $title = 'Trashed Trainings';

        $trainingsCount = $this->trainingRepo->search($request);

        $request['trashed'] = true;
        $trainings = $this->trainingRepo->search($request);

        return view('admin.training.trashed', compact('trainings','title', 'trainingsCount'));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->trainingRepo->doBulkRemoveTraining($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('Trainings deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    public function recover($training)
    {
        $training->restore();
        $training->audit('recover');
        $this->trainingRepo->restoreRegistrations($training);

        return redirect()->route('admin.trainings')
            ->withSuccess(__('validation.request.restore', ['prefix' => "Training "]));
    }

    public function destroyPermanent($training)
    {
        $title = $training->title;
        $this->trainingRepo->deletePermanent($training);

        return redirect()->route('admin.trainings.trashed')
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Training '$title'"]));
    }
}
