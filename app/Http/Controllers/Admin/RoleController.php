<?php

namespace App\Http\Controllers\Admin;

use Ayala\Employee\Role;
use Ayala\Employee\RoleRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function __construct(
        RoleRepository $roleRepo
    )
    {
        $this->roleRepo = $roleRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Roles';

        $roles = $this->roleRepo->search($request)->sortBy('title');

        $query = [
            'search' => $request->search,
            'per_page' => $request->per_page
        ];

        return view('admin.role.index', compact('title', 'roles', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create Role';

        return view('admin.role.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $role = $this->roleRepo->addOrUpdateRole($request);

            return redirect()->route('admin.roles')
                ->withSuccess(__('validation.request.create', ['prefix' => "Role '$role->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $title = 'Edit Role';

        return view('admin.role.edit', compact('title', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        try {
            $role = $this->roleRepo->addOrUpdateRole($request, $role);

            return redirect()->route('admin.roles')
                ->withSuccess(__('validation.request.update', ['prefix' => "Role '$role->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        // To create logs here
        $title = $role->title;
        $role->audit('delete');
        $role->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Role '$title'"]));
    }
}
