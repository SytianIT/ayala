<?php

namespace App\Http\Controllers\Admin;

use Ayala\Jobs\Department;
use Ayala\Jobs\DepartmentRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    public function __construct(
        DepartmentRepository $departmentRepo)
    {
        $this->departmentRepo = $departmentRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Departments';

        $departments = $this->departmentRepo->search($request, ['jobs'])->sortBy('title');
        $trashedCount = $this->departmentRepo->getTrashed()->count();

        $query = [
            'search' => $request->search,
            'per_page' => $request->per_page
        ];

        return view('admin.department.index', compact('title', 'departments', 'trashedCount', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create Department';

        return view('admin.department.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $department = $this->departmentRepo->addDepartment($request);

            return redirect()->route('admin.jobs.departments')
                ->withSuccess(__('validation.request.create', ['prefix' => "Department '$department->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        $title = 'Edit Department';

        return view('admin.department.edit', compact('title', 'department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        try {
            $department = $this->departmentRepo->updateDepartment($request, $department);

            return redirect()->route('admin.jobs.departments')
                ->withSuccess(__('validation.request.update', ['prefix' => "Department '$department->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        // To create logs here
        $title = $department->title;
        $department->audit('delete');
        $department->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Department '$title'"]));
    }

    /**
     * Display a listing of the resource (trashed).
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $title = 'Trashed Departments';

        $departmentsCount = $this->departmentRepo->search($request);

        $request['trashed'] = true;
        $departments = $this->departmentRepo->search($request);

        return view('admin.department.trashed', compact('departments','title', 'departmentsCount'));
    }

    public function recover($department)
    {
        $department = Department::withTrashed()->where('id', $department)->restore();

        return redirect()->route('admin.jobs.departments')
            ->withSuccess(__('validation.request.restore', ['prefix' => "Department "]));
    }

    public function destroyPermanent($department)
    {
        $department = Department::withTrashed()->where('id', $department)->first();
        $title = $department->title;
        $department->forcedelete();

        return redirect()->route('admin.jobs.departments.trashed')
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Training '$title'"]));
    }
}
