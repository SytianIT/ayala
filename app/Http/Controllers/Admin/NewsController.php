<?php

namespace App\Http\Controllers\Admin;

use Ayala\News\Category;
use Ayala\News\CategoryRepository;
use Ayala\News\News;
use Ayala\News\NewsRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function __construct(
        NewsRepository $newsRepo,
        CategoryRepository $categoryRepo
    )
    {
        $this->newsRepo = $newsRepo;
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'News & Events';

        $newsCollect = $this->newsRepo->search($request, ['categories']);
        $trashedCount = $this->newsRepo->getTrashed()->count();

        $query = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_data,
            'search' => $request->search,
            'per_page' => $request->per_page
        ];

        return view('admin.news.index', compact('title', 'newsCollect', 'trashedCount', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create News & Events';

        $categories = Category::getNestedList('title', 'id', ' -- ');
        $newsCollection = $this->newsRepo->search($request, ['categories','related']);

        return view('admin.news.create', compact('title', 'categories', 'newsCollection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $news = $this->newsRepo->addNews($request);

            return redirect()->route('admin.news')
                ->withSuccess(__('validation.request.create', ['prefix' => "News & Events '$news->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, News $news)
    {
        $title = 'Edit News & Events';

        $categories = Category::getNestedList('title', 'id', ' -- ');
        $request['except_id'] = $news->id;
        $newsCollection = $this->newsRepo->search($request, ['categories','related']);

        return view('admin.news.edit', compact('title', 'categories', 'news', 'newsCollection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        try {
            $news = $this->newsRepo->updateNews($request, $news);

            return redirect()->route('admin.news')
                ->withSuccess(__('validation.request.update', ['prefix' => "News & Events '$news->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        // To create logs here
        $title = $news->title;
        $news->audit('delete');
        $news->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "News & Events '$title'"]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->newsRepo->doBulkRemove($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('News & Events deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    /**
     * Display a listing of the resource (trashed).
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $title = 'Trashed News & Events';

        $newsCount = $this->newsRepo->search($request);

        $request['trashed'] = true;
        $newsCollect = $this->newsRepo->search($request);

        return view('admin.news.trashed', compact('newsCollect','title', 'newsCount'));
    }

    public function recover($news) 
    {
        $news = News::withTrashed()->where('id', $news)->restore();

        return redirect()->route('admin.news')
            ->withSuccess(__('validation.request.restore', ['prefix' => "News & Events "]));
    }

    public function destroyPermanent($news)
    {
        $news = News::withTrashed()->where('id', $news)->first();
        $title = $news->title;
        $news->forcedelete();

        return redirect()->route('admin.news.trashed')
            ->withSuccess(__('validation.request.deleted', ['prefix' => "News & Events '$title'"]));
    }
}
