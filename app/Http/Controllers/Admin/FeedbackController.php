<?php

namespace App\Http\Controllers\Admin;

use Ayala\Feedback\Feedback;
use Ayala\Feedback\FeedbackRepository;
use Ayala\Jobs\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    public function __construct(
        FeedbackRepository $feedbackRepo,
        Department $department
    )
    {
        $this->feedRepo = $feedbackRepo;
        $this->department = $department;
    }

    public function index(Request $request)
    {
        $title = 'Feedback';

        $request['status'] = Feedback::STATS_NEW;
        $feedbacks = $this->feedRepo->search($request, ['department']);

        $archFeedbacks = $this->feedRepo->getArchives()->count();

        $query = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_data,
            'department' => $request->department,
            'feed_status' => $request->status,
            'per_page' => $request->per_page ?: self::PER_PAGE
        ];

        return view('admin.feedback.index', compact('feedbacks', 'departments', 'query', 'title', 'archFeedbacks'));
    }

    public function show(Feedback $feedback)
    {
        $title = 'View Feedback';

        return view('admin.feedback.show', compact('feedback','title'));
    }

    public function archives(Request $request)
    {
        $title = 'Feedback Archives';

        $request['status'] = Feedback::STATS_NEW;
        $newFeedbacks = $this->feedRepo->search($request, ['department']);

        $request['status'] = Feedback::STATS_PROCESSED;
        $feedbacks = $this->feedRepo->search($request, ['department']);

        $query = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_data,
            'department' => $request->department,
            'per_page' => $request->per_page ?: self::PER_PAGE
        ];

        return view('admin.feedback.archive', compact('feedbacks', 'departments', 'query', 'title', 'newFeedbacks'));
    }

    public function makeArchive(Feedback $feedback)
    {
        $feedback->status = Feedback::STATS_PROCESSED;
        $feedback->save();
        $feedback->audit('update');

        return redirect()->back()
            ->withSuccess(__('validation.request.update', ['prefix' => "Feedback "]));
    }

    public function destroy(Feedback $feedback)
    {
        // To create logs here
        $title = $feedback->department ? 'Feedback for department ' . $feedback->department->title : 'Feedback ' . $feedback->created_at->toDateTimeString();
        $feedback->audit('delete');
        $feedback->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => $title]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->feedRepo->doBulkRemove($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('Feedbacks deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }
}
