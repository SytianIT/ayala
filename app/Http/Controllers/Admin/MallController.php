<?php

namespace App\Http\Controllers\Admin;

use Ayala\Employee\Mall;
use Ayala\Employee\MallRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MallController extends Controller
{
    public function __construct(
        MallRepository $mallRepo
    )
    {
        $this->mallRepo = $mallRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Malls';

        $malls = $this->mallRepo->search($request)->sortBy('title');

        $query = [
            'search' => $request->search,
            'per_page' => $request->per_page
        ];

        return view('admin.mall.index', compact('title', 'malls', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create Mall';

        return view('admin.mall.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $mall = $this->mallRepo->addOrUpdateMall($request);

            return redirect()->route('admin.malls')
                ->withSuccess(__('validation.request.create', ['prefix' => "Mall '$mall->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mall $mall)
    {
        $title = 'Edit Mall';

        return view('admin.mall.edit', compact('title', 'mall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mall $mall)
    {
        try {
            $mall = $this->mallRepo->addOrUpdateMall($request, $mall);

            return redirect()->route('admin.malls')
                ->withSuccess(__('validation.request.update', ['prefix' => "Mall '$mall->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mall $mall)
    {
        // To create logs here
        $title = $mall->title;
        $mall->audit('delete');
        $mall->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Mall '$title'"]));
    }
}
