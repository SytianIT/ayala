<?php

namespace App\Http\Controllers\Admin;

use Ayala\Icons\Icon;
use Ayala\Icons\IconRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobIconsController extends Controller
{
    public function __construct(
        IconRepository $iconRepo
    )
    {
        $this->iconRepo = $iconRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Icons';

        $icons = $this->iconRepo->search($request);

        return view('admin.job.icon.index', compact('title', 'icons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create Icon';

        return view('admin.job.icon.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $icon = $this->iconRepo->addOrUpdateIcon($request);

            return redirect()->route('admin.icons')
                ->withSuccess(__('validation.request.create', ['prefix' => "Icon '$icon->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Icon $icon)
    {
        $title = 'Edit Icon';

        return view('admin.job.icon.edit', compact('title', 'icon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icon $icon)
    {
        try {
            $icon = $this->iconRepo->addOrUpdateIcon($request, $icon);

            return redirect()->route('admin.icons')
                ->withSuccess(__('validation.request.update', ['prefix' => "Icon '$icon->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Icon $icon)
    {
        // To create logs here
        $title = $icon->title;
        $icon->delete();

        $icon->audit('delete');
        $icon->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Icon '$title'"]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->iconRepo->doBulkRemove($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('Icons deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }
}
