<?php

namespace App\Http\Controllers\Admin;

use Ayala\Audit\AuditRepository;
use Ayala\SiteSettings\SiteSettings;
use Ayala\Uploadable\UploadableRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\User;
use Ayala\News\News;
use Ayala\Jobs\Job;
use Ayala\Training\Training;
use Ayala\Learning\Learning;
use Ayala\Survey\Survey;
use Ayala\Registration\Registration;
use Ayala\Feedback\Feedback;

class SettingsController extends Controller
{
    public function __construct(
        SiteSettings $siteSettings,
        UploadableRepository $uploadRepo,
        AuditRepository $auditRepo
    )
    {
        $this->siteSettings = $siteSettings;
        $this->uploadRepo = $uploadRepo;
        $this->auditRepo = $auditRepo;
    }

    public function settings()
    {
        $title = "Site Settings";

        $settings = $this->siteSettings->settings();
        $sliders = $settings->uploads->sortBy('order');

        return view('admin.others.settings', compact('settings','sliders', 'title'));
    }

    public function saveSettings(Request $request)
    {
        try {
            $validator = \Validator::make($request->all(), [
                'site_name' => 'required',
                'site_email' => 'required|email'
            ]);

            if ($validator->fails()) {
                throw new \Ayala\Validation\ValidationException($validator->messages());
            }

            $settings = $this->siteSettings->settings();
            $settings->site_name = $request->site_name;
            $settings->site_email = $request->site_email;
            $settings->other_data = (array) $request->other_data;
            $settings->save();
            $settings->audit('mass');

            if ($request->slider && count($request->slider) > 0) {
                $this->uploadMedia($request, $settings);
            } else {
                $this->deleteSliders($request, $settings, true);
            }

            return redirect()->back()
                    ->withSuccess('Site settings updated succesfully!');
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    private function uploadMedia($request, $settings)
    {
        $this->deleteSliders($request, $settings);

        $ctr = 1;
        foreach ($request->slider as $key => $data) {
            if ((!isset($data['file']) && isset($data['id'])) || (isset($data['file']) && $data['file'] instanceof UploadedFile)) {
                $request['upload_id'] = isset($data['id']) ? $data['id'] : null;
                $request['file'] = isset($data['file']) ? $data['file'] : null;
                $request['order'] = $ctr;
                $this->uploadRepo->createUpload($settings, $request, 'image', [
                    'key' => SiteSettings::IMG_SLIDER,
                    'field_key' => 'file',
                    'path' => 'uploads/settings',
                    'fileName' => 'file',
                    'multiple' => true
                ]);
                $ctr++;
            }
        }
    }

    private function deleteSliders($request, $settings, $all = false)
    {
        if ($all) {
            $settings->uploads()->delete();
        } else {
            $deleting = array_map(function($arr) {
                return isset($arr['id']) ? $arr['id'] : null;
            }, $request->slider);
            $settings->uploads()->whereNotIn('id', $deleting)->delete();
        }
    }

    // Extras Controller
    public function audits(Request $request)
    {
        $title = 'Audit Logs';

        $request['per_page'] = $request->per_page ?: self::PER_PAGE;
        $audits = $this->auditRepo->search($request, ['auditable']);

        $query = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_data,
            'per_page' => $request->per_page ?: self::PER_PAGE
        ];

        return view('admin.others.audits', compact('audits','query'));
    }

    public function preview() 
    {
        $employee = User::find(1);
        $news = News::latest()->first();

        $job = Job::latest()->first();
        $recipient = $job->getOtherData('recipient');
        $recipients = explode(', ', $recipient);
        $supervisor = $job->getOtherData('supervisor');
        $registration = $job->registrations->first();

        $learning = Learning::latest()->first();
        $training = Training::latest()->first();

        $jobImageURL = asset($job->img_thumb_path);
        $newsImageURL = asset($news->img_thumb_path);
        $trainingImageURL = asset($training->img_thumb_path);
        $learningImageURL = asset($learning->img_thumb_path);

        $survey = Survey::latest()->first();
        $registration = Registration::latest()->first();
        $feedback = Feedback::with('department')->latest()->first();

        return view('admin.email.new-news-events', compact('news', 'job', 'learning', 'training','employee', 'survey', 'registration', 'feedback', 'recipient', 'recipients', 'supervisor', 'registration', 'jobImageURL', 'newsImageURL', 'trainingImageURL', 'learningImageURL'));

    }
}

