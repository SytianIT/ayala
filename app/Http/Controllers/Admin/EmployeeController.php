<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Ayala\Employee\Employee;
use Ayala\Employee\EmployeeRepository;
use Ayala\Employee\MallRepository;
use Ayala\Employee\RoleRepository;
use Ayala\Jobs\DepartmentRepository;
use Ayala\User\UserRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    public function __construct(
        UserRepository $userRepo,
        DepartmentRepository $departmentRepo,
        RoleRepository $roleRepo,
        MallRepository $mallRepo,
        EmployeeRepository $employeeRepo
    )
    {
        $this->userRepo = $userRepo;
        $this->departmentRepo = $departmentRepo;
        $this->roleRepo = $roleRepo;
        $this->mallRepo = $mallRepo;
        $this->employeeRepo = $employeeRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Employees';

        $request['per_page'] = $request->per_page ?: self::PER_PAGE;
        $request['employees'] = true;
        $employees = $this->userRepo->search($request, ['employee.surveys']);

        $request['trashed'] = true;
        $trashedCount = $this->userRepo->search($request)->count();

        $query = [
            'per_page' => $request->per_page,
            'name' => $request->name
        ];

        return view('admin.employee.index', compact('employees','title', 'trashedCount', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create Employee';

        $departments = $this->departmentRepo->search($request);
        $roles = $this->roleRepo->search($request);
        $malls = $this->mallRepo->search($request);

        return view('admin.employee.create', compact('title', 'departments', 'roles', 'malls'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = $this->userRepo->addUser($request);

            return redirect()->route('admin.employees')
                ->withSuccess(__('validation.request.create', ['prefix' => "Employee '$user->username'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $employee)
    {
        $title = 'Edit Employee';

        $departments = $this->departmentRepo->search($request);
        $roles = $this->roleRepo->search($request);
        $malls = $this->mallRepo->search($request);

        return view('admin.employee.edit', compact('employee', 'title', 'departments', 'roles', 'malls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $employee)
    {
        try {
            $user = $this->userRepo->updateUser($request, $employee);

            return redirect()->route('admin.employees')
                ->withSuccess(__('validation.request.update', ['prefix' => "Employee '$user->username'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $employee)
    {
        // To create logs here
        $username = $employee->username;
        $this->employeeRepo->deleteEmployee($employee->employee);
        $employee->delete(); // Use object delete

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Employee '$username'"]));
    }

    public function bulkRemove(Request $request)
    {
        if ($request->filled('selected')) {
            $ids = explode(',', $request->selected);
            foreach ($ids as $id) {
                $user = User::withTrashed()->with([
                    'employee' => function($employee) {
                        return $employee->withTrashed();
                    }
                ])->find($id);
                if ($user) {
                    if ($user->trashed()) {
                        $user->employee->forceDelete();
                        $user->forceDelete();
                    } else {
                        $this->employeeRepo->deleteEmployee($user->employee);
                        $user->delete();
                    }
                }
            }

            return redirect()->back()
                ->withSuccess('Employees deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    /**
     * Display a listing of the resource (trashed).
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $title = 'Trashed Employees';

        $request['employees'] = true;
        $employeesCount = $this->userRepo->search($request)->count();

        $request['trashed'] = true;
        $employees = $this->userRepo->search($request);

        return view('admin.employee.trashed', compact('employees','title', 'employeesCount'));
    }

    public function recover($employee)
    {
        $employee = User::withTrashed()->where('id', $employee)->with([
            'employee' => function($employee) {
                return $employee->withTrashed();
            }
        ])->first();

        $employee->restore();
        $employee->employee->restore();

        if ($employee->employee->department) {
            $employee->employee->department()->withTrashed()->restore();
        }

        return redirect()->route('admin.employees')
            ->withSuccess(__('validation.request.restore', ['prefix' => "Employee "]));
    }

    public function destroyPermanent($employee)
    {
        $employee = User::withTrashed()->where('id', $employee)->with([
            'employee' => function($employee) {
                return $employee->withTrashed();
            }
        ])->first();

        $name = $employee->username;
        $employee->employee->forcedelete();
        $employee->forcedelete();

        return redirect()->route('admin.employees.trashed')
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Employee '$name'"]));
    }

    public function import()
    {
        return view('admin.employee.import');
    }

    public function load(Request $request)
    {
        try {
            $data = $this->employeeRepo->loadExcel($request, true);

            return view('admin.employee.inc.excel-prepared', compact('data'));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e, true);
        }
    }

    public function doImport(Request $request)
    {
        try {
            $data = $this->employeeRepo->loadExcel($request);

            return redirect()->route('admin.employees')
                ->withSuccess('Import success!');
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
