<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Ayala\Role\Role;
use Ayala\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ayala\Validation\ValidationException;

class AdminController extends Controller
{
    public function __construct(
        UserRepository $userRepo
    )
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Admins';

        $request['admins'] = true;
        $admins = $this->userRepo->search($request);

        $request['trashed'] = true;
        $trashedCount = $this->userRepo->search($request)->count();

        return view('admin.admins.index', compact('admins','title', 'trashedCount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create Admin';
        $rights = Role::whereIn('id', Role::$adminAccess)->get();

        return view('admin.admins.create', compact('title', 'rights'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = $this->userRepo->addUser($request, true);

            return redirect()->route('admin.admins')
                ->withSuccess(__('validation.request.create', ['prefix' => "Admin '$user->username'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $admin)
    {
        $title = 'Edit Admin';
        $rights = Role::whereIn('id', Role::$adminAccess)->get();

        return view('admin.admins.edit', compact('admin', 'title', 'rights'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $admin)
    {
        try {
            $user = $this->userRepo->updateUser($request, $admin, true);

            return redirect()->route('admin.admins')
                ->withSuccess(__('validation.request.update', ['prefix' => "Admin '$user->username'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $admin)
    {
        // To create logs here
        $username = $admin->username;
        $admin->audit('delete');
        $admin->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Admin '$username'"]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->userRepo->doBulkRemove($request);
        if ($response) {
            return redirect()->back()
                ->withSuccess('Admins deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    /**
     * Display a listing of the resource (trashed).
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $title = 'Trashed Admins';

        $request['admins'] = true;
        $adminsCount = $this->userRepo->search($request)->count();

        $request['trashed'] = true;
        $admins = $this->userRepo->search($request);

        return view('admin.admins.trashed', compact('admins','title', 'adminsCount'));
    }

    public function recover($admin)
    {
        $admin = User::withTrashed()->where('id', $admin)->restore();

        return redirect()->route('admin.admins')
            ->withSuccess(__('validation.request.restore', ['prefix' => "Admin "]));
    }

    public function destroyPermanent($admin)
    {
        $admin = User::withTrashed()->where('id', $admin)->first();
        $name = $admin->username;
        $admin->forcedelete();

        return redirect()->route('admin.admins.trashed')
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Admin '$name'"]));
    }
}
