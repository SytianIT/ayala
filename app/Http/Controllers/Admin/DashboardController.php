<?php

namespace App\Http\Controllers\Admin;

use Ayala\Audit\AuditRepository;
use Ayala\Feedback\FeedbackRepository;
use Ayala\Jobs\JobRepository;
use Ayala\Registration\Registration;
use Ayala\Registration\RegistrationRepository;
use Ayala\Survey\Survey;
use Ayala\Survey\SurveyAnswersRepository;
use Ayala\Survey\SurveyRepository;
use Ayala\Training\TrainingRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct(
        RegistrationRepository $registrationRepo,
        FeedbackRepository $feedbackRepo,
        JobRepository $jobRepository,
        TrainingRepository $trainingRepo,
        AuditRepository $auditRepo,
        SurveyRepository $surveyRepo,
        SurveyAnswersRepository $surveyAnswersRepo
    )
    {
        $this->middleware('admin')->except('login');
        $this->registrationRepo = $registrationRepo;
        $this->feedbackRepo = $feedbackRepo;
        $this->jobRepo = $jobRepository;
        $this->trainingRepo = $trainingRepo;
        $this->auditRepo = $auditRepo;
        $this->surveyAnswersRepo = $surveyAnswersRepo;
        $this->surveyRepo = $surveyRepo;
    }

    public function index(Request $request)
    {
        $request['active'] = true;

        $jobOpenings = $this->jobRepo->search($request, ['registrations']);
        $trainings = $this->trainingRepo->search($request, ['registrations']);
        $availableTrainings = $trainings->filter(function ($q) {
            return $q->slots > $q->registrations->count();
        });

        $request['from_date'] = Carbon::today()->startOfMonth();
        $request['to_date'] = Carbon::today()->endOfMonth();
        $feedback = $this->feedbackRepo->search($request);

        $request['trainings'] = true;
        $registrations = $this->registrationRepo->search($request);
        unset($request['trainings']);

        $request['jobs'] = true;
        $applications = $this->registrationRepo->search($request);
        unset($request['jobs']);

        unset($request['from_date']);
        unset($request['to_date']);
        $request['status'] = Registration::STATS_NEW;
        $newFeedback = $this->feedbackRepo->search($request, ['department']);

        $request['trainings'] = true;
        $newRegistrations = $this->registrationRepo->search($request, ['relatable']);
        unset($request['trainings']);

        $request['jobs'] = true;
        $newApplications = $this->registrationRepo->search($request, ['relatable']);

        $request['dashboard'] = true;
        $audits = $this->auditRepo->search($request, ['auditable']);

        $request['status'] = Survey::STATS_COMPLETED;
        $request['dashboard'] = true;
        $surveyAnswers = $this->surveyAnswersRepo->search($request, ['survey', 'employee']);

        unset($request['status']);
        $request['active'] = true;
        $activeSurvey = $this->surveyRepo->search($request);

        return view('admin.index', compact('jobOpenings', 'availableTrainings', 'feedback', 'registrations', 'applications', 'newFeedback', 'newRegistrations', 'newApplications', 'audits', 'surveyAnswers', 'activeSurvey'));
    }
}
