<?php

namespace App\Http\Controllers\Admin;

use Ayala\Training\Category;
use Ayala\Training\CategoryRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainingCategoryController extends Controller
{
    public function __construct(
        CategoryRepository $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Training Categories';

        $request['roots'] = true;
        $categories = Category::roots()->latest()->get();

        return view('admin.training.category.index', compact('title','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create Training Category';
        $categories = Category::getNestedList('title', 'id', ' -- ');

        return view('admin.training.category.create', compact('title', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $category = $this->categoryRepo->addOrUpdateCategory($request);

            return redirect()->route('admin.trainings.categories')
                ->withSuccess(__('validation.request.create', ['prefix' => "Category '$category->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $title = 'Edit Training Category';

        $categories = Category::getNestedList('title', 'id', ' -- ', $category->id);

        return view('admin.training.category.edit', compact('title', 'categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        try {
            $category = $this->categoryRepo->addOrUpdateCategory($request, $category);

            return redirect()->route('admin.trainings.categories')
                ->withSuccess(__('validation.request.update', ['prefix' => "Category '$category->title'"]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $title = $category->title;
        $category->audit('delete');
        $category->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Category '$title'"]));
    }
}
