<?php

namespace App\Http\Controllers\Admin;

use Ayala\Employee\Employee;
use Ayala\Employee\Mall;
use Ayala\Employee\MallRepository;
use Ayala\Employee\Role;
use Ayala\Employee\RoleRepository;
use Ayala\FileGenerator;
use Ayala\Jobs\Department;
use Ayala\Survey\Survey;
use Ayala\Survey\SurveyAnswersRepository;
use Ayala\Survey\SurveyItems;
use Ayala\Survey\SurveyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveyController extends Controller
{
    public function __construct(
        SurveyRepository $surveyRepo,
        SurveyAnswersRepository $surveyAnswerRepo,
        MallRepository $mallRepo,
        RoleRepository $roleRepo,
        Mall $mall,
        Department $department,
        Role $role
    )
    {
        $this->surveyRepo = $surveyRepo;
        $this->mallRepo = $mallRepo;
        $this->roleRepo = $roleRepo;
        $this->mall = $mall;
        $this->department = $department;
        $this->role = $role;
        $this->surveyAnswerRepo = $surveyAnswerRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Survey';

        $surveys = $this->surveyRepo->search($request, ['items', 'malls', 'roles', 'departments']);
        $trashedCount = $this->surveyRepo->getTrashed()->count();

        $query = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_data,
            'per_page' => $request->per_page ?: self::PER_PAGE,
            'name' => $request->name,
            'survey_status' => $request->status
        ];

        return view('admin.survey.index', compact('surveys','trashedCount', 'title', 'query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create Survey';

        $malls = $this->mall->getFomattedArray(['label', 'value'], ['title', 'id']);
        $roles = $this->role->getFomattedArray(['label', 'value'], ['title', 'id']);
        $departments = $this->department->getFomattedArray(['label', 'value'], ['title', 'id']);
        $items = SurveyItems::$types;

        return view('admin.survey.create', compact('malls', 'roles', 'items', 'departments', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Survey $survey)
    {
        $title = 'Edit Survey';

        $survey->load(['malls','departments','roles','employees']);
        $survey->items = $survey->items->sortBy('order');
        $malls = $this->mall->getFomattedArray(['label', 'value'], ['title', 'id']);
        $roles = $this->role->getFomattedArray(['label', 'value'], ['title', 'id']);
        $departments = $this->department->getFomattedArray(['label', 'value'], ['title', 'id']);
        $items = SurveyItems::$types;

        return view('admin.survey.edit', compact('survey', 'malls', 'roles', 'items', 'departments', 'selectedMalls', 'selectedDepartments', 'selectedRoles', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Survey $survey)
    {
        $title = $survey->title;
        $survey->audit('delete');
        $survey->delete();

        return redirect()->back()
            ->withWarning(__('validation.request.deleted', ['prefix' => "Survey '$title'"]));
    }

    public function bulkRemove(Request $request)
    {
        $response = $this->surveyRepo->doBulkRemove($request);
        if ($response) {
            return redirect()->back()
                             ->withSuccess('Items deleted successfully!');
        }

        return redirect()->back()
                         ->withErrors('There was an error in processing the request, please try again later.');
    }

    /**
     * Display a listing of the resource (trashed).
     *
     * @return \Illuminate\Http\Response
     */
    public function trashed(Request $request)
    {
        $title = 'Trashed Surveys';

        $surveysCount = $this->surveyRepo->search($request);

        $request['trashed'] = true;
        $surveys = $this->surveyRepo->search($request);

        return view('admin.survey.trashed', compact('surveys','title', 'surveysCount'));
    }

    public function recover(Survey $survey)
    {
        $survey->restore();
        $this->surveyRepo->restoreRelations($survey);
        $survey->audit('recover');

        return redirect()->route('admin.survey')
            ->withSuccess(__('validation.request.restore', ['prefix' => "Survey "]));
    }

    public function destroyPermanent(Survey $survey)
    {
        $title = $survey->title;
        $this->surveyRepo->deletePermanent($survey);

        return redirect()->route('admin.survey.trashed')
            ->withSuccess(__('validation.request.deleted', ['prefix' => "Survey '$title'"]));
    }
    
    /**
     * Start methods for answers
     */
    public function answers(Request $request, Survey $survey)
    {
        $title = 'Survey Employee Answers';
        $employees = $survey->employees;

        $request['survey_id'] = $survey->id;
        $employees = $this->surveyAnswerRepo->search($request, ['survey', 'employee']);

        $query = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_data,
            'per_page' => $request->per_page ?: self::PER_PAGE,
            'status' => $request->status,
            'name' => $request->name
        ];

        return view('admin.survey.answer.index', compact('survey','title', 'employees', 'query'));
    }

    public function statistic(Survey $survey)
    {
        $title = 'Survey Statistics';

        $statistics = $this->surveyRepo->getStatistics($survey);
        $survey->load([
            'items' => function($q) {
                $q->whereIn('type',[SurveyItems::TYPE_CHECKBOX, SurveyItems::TYPE_RADIO]);
            }
        ]);

        return view('admin.survey.answer.statistic', compact('survey','title', 'statistics'));
    }

    public function exportStatistic(Survey $survey)
    {
        $statistics = $this->surveyRepo->getStatistics($survey);
        $survey->load([
            'items' => function($q) {
                $q->whereIn('type',[SurveyItems::TYPE_CHECKBOX, SurveyItems::TYPE_RADIO]);
            }
        ]);

        FileGenerator::makeExcel(
            '',
            'Survey - ' . $survey->title . ' Statistics',
            'Statistics',
            'admin.export.survey-statistic', compact('statistics','survey')
        );
    }

    public function showAnswer(Survey $survey, Employee $employee)
    {
        $title = 'Survey Employee Answer';

        $ids = $survey->items->pluck('id')->toArray();
        $answers = $employee->answers()->whereIn('survey_item_id', $ids)->get();
        $employee = $survey->employees()->find($employee->id);

        return view('admin.survey.answer.show', compact('survey','title', 'employee', 'answers'));
    }

    public function removeAnswer(Survey $survey, Employee $employee)
    {
        $answer = $this->surveyAnswerRepo->deleteAnswer($survey, $employee);

        return redirect()->back()
            ->withSuccess('Survey answer delete succesfully!');
    }

    public function bulkRemoveAnswer(Request $request, Survey $survey)
    {
        $response = $this->surveyAnswerRepo->myOwnDoBulkRemove($request, $survey);

        if ($response) {
            return redirect()->back()
                ->withSuccess('Answers deleted successfully!');
        }

        return redirect()->back()
            ->withErrors('There was an error in processing the request, please try again later.');
    }

    public function processedAnswer(Survey $survey, Employee $employee)
    {
        $survey->employees()->updateExistingPivot($employee->id, ['status' => Survey::STATS_PROCESSED]);

        return redirect()->back()
                         ->withSuccess('Survey answer processed succesfully!');
    }
}
