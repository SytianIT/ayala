<?php

namespace App\Http\Controllers\Admin;

use Ayala\User\UserRepository;
use Ayala\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function __construct(
        UserRepository $userRepo
    )
    {
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        return view('admin.account.index');
    }

    public function update(Request $request)
    {
        try {
            $user = $request->user();
            $user = $this->userRepo->updateUser($request, $user, true);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => "Account "]));
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
