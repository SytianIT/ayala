<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Ayala\User\UserRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        UserRepository $userRepo)
    {
        $this->middleware('guest')->except('logout');
        $this->userRepo = $userRepo;
    }

    public function showLoginForm()
    {
        return redirect(route('home'));
    }

    /**
     * Try to override Login method
     * @param Request $request
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response|void
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $errors = [];
        $admin = $request->has("magic") ? true : false;
        $username = $request->username;
        $user = $this->userRepo->getFirstBy('username', $username);

        if ($user) {
            // Compare now the password
            if (\Hash::check($request->password, $user->password)) {
                if ($admin) {
                    if ($user->isAdmin()) {
                        Auth::login($user);
                        return $this->sendLoginResponse($request);
                    }
                }

                if (!$admin && $user->isUser()) {
                    Auth::login($user);
                    return $this->sendLoginResponse($request);
                }

                $errors['message'] = 'There is no account associated with that credentials';
            } else {
                $errors['password'] = 'Invalid password';
            }
        } else {
            $errors['username'] = 'Invalid username';
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request, $errors);
    }

    public function authenticated(Request $request, User $user)
    {
        if ($user->isAdmin()) {
            return redirect()->route('admin.dashboard');
        }

        if ($request->ajax()) {
            return response()->json(['success' => true, 'redirect' => $this->redirectPath()]);
        }

        return redirect()->intended($this->redirectPath());
    }

    public function username()
    {
        return 'username';
    }

    protected function sendFailedLoginResponse(Request $request, $errors = [])
    {
        if ($request->ajax()) {
            return response()->json(['success' => false, 'errors' => $errors]);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'password', 'remember'))
            ->withErrors($errors);
    }
}
