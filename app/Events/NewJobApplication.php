<?php

namespace App\Events;

use Ayala\Jobs\Job;
use Ayala\Registration\Registration;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewJobApplication
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $job;
    public $registration;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Job $job, Registration $registration)
    {
        $this->job = $job;
        $this->registration = $registration;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
