<?php

namespace App\Events;

use Ayala\Training\Training;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use \Ayala\Registration\Registration as RegistrationClass;

class Registration
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $training;
    public $registration;

    /**
     * Create a new event instance.
     *
     * @param Training $training
     * @param Registration $registration
     */
    public function __construct(Training $training, RegistrationClass $registration)
    {
        $this->training = $training;
        $this->registration = $registration;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
