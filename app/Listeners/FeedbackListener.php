<?php

namespace App\Listeners;

use App\Events\Feedback;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class FeedbackListener
{
    /** 
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Feedback  $event
     * @return void
     */
    public function handle(Feedback $event)
    {
        $feedback = $event->feedback;
        $settings = SiteSettings::getSettings();

        if ($settings) {
            $data = [
                'feedback' => $feedback,
            ];

            if(filter_var($settings->site_email, FILTER_VALIDATE_EMAIL)) {
                $this->mailer->send('admin.email.feedback', $data, function ($m) use ($feedback, $settings) {
                    $m->from(env('SENDING_DOMAIN'), $feedback->department ? $feedback->department->title : 'Anonymous');
                    $m->to($settings->site_email, $settings->site_name)->subject($feedback->department ? "Feedback from department '{$feedback->department->title}'" : 'New Feedback Receive');
                });
            }
        }

        if ($feedback->department) {
            $message = "An email notification of feedback from '{$feedback->department->title}' has failed to send. Cause: No email registered!";
        } else {
            $message = "An email notification of feedback has failed to send. Cause: No email registered!";
        }
        Log::alert($message);
    }
}
