<?php

namespace App\Listeners;

use App\Events\Registration;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class RegistrationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(Registration $event)
    {
        $training = $event->training;
        $settings = SiteSettings::getSettings();
        $registration = $event->registration;
 
        if ($settings) {
            $data = [
                'training' => $training,
                'registration' => $registration,
            ];

            if(filter_var($settings->site_email, FILTER_VALIDATE_EMAIL)) {
                $this->mailer->send('admin.email.training-registration', $data, function ($m) use ($training, $settings, $registration) {
                    $m->from(env('SENDING_DOMAIN'), 'Ayala Mall - Feedback Site');
                    $m->to($settings->site_email, $settings->site_name)->subject("Registration for training '{$training->title}'");
                });
            }
        } else {
            Log::alert("An email notification of training registration for '{$training->title}' has failed to send. Cause: No email registered!");
        }
    }
}
