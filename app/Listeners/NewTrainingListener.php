<?php

namespace App\Listeners;

use App\Events\NewTraining;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;

use Ayala\Jobs\Job;
use Ayala\Learning\Learning;
use Ayala\News\News;

class NewTrainingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  NewTraining  $event
     * @return void
     */
    public function handle(NewTraining $event)
    {
        $training = $event->training;
        $employee = $event->employee;
        $settings = SiteSettings::getSettings();

        $news = News::inRandomOrder()->first();
        $job = Job::inRandomOrder()->first();
        $learning = Learning::inRandomOrder()->first();

        $jobImageURL = $job ? asset($job->img_thumb_path) : null;
        $newsImageURL = $news ? asset($news->img_thumb_path) : null;
        $learningImageURL = $learning ? asset($learning->img_thumb_path) : null;
        $trainingImageURL = $training ? asset($training->img_thumb_path) : null;

        $data = [
            'employee' => $employee,
            'news' => $news,
            'job' => $job,
            'learning' => $learning,
            'training' => $training,
            'jobImageURL' => $jobImageURL,
            'newsImageURL' => $newsImageURL,
            'learningImageURL' => $learningImageURL,
            'trainingImageURL' => $trainingImageURL,
        ];

        if(filter_var($employee->email, FILTER_VALIDATE_EMAIL)) {
            $this->mailer->send('admin.email.new-training', $data, function ($m) use ($training, $employee, $settings, $news, $job, $learning, $jobImageURL, $newsImageURL, $learningImageURL, $trainingImageURL) {
                $m->from(env('SENDING_DOMAIN'), 'Ayala Malls - Feedback');
                $m->to(env('APP_TESTING') ? env('APP_TESTING_EMAIL') : $employee->email, $employee->first_name)->subject($training->title);
            });
        }
    }
}
