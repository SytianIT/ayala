<?php

namespace App\Listeners;

use App\Events\NewJobApplication;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log; 

class NewJobApplicationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  NewJobApplication  $event
     * @return void
     */
    public function handle(NewJobApplication $event)
    { 
        $job = $event->job;
        $settings = SiteSettings::getSettings();
        $recipient = $job->getOtherData('recipient');
        $recipients = explode(', ', $recipient);
        $supervisor = $job->getOtherData('supervisor');
        $registration = $event->registration;

        if ($recipient && $recipients) {
            $data = [
                'job' => $job,
                'registration' => $registration,
                'supervisor' => $supervisor
            ];

            foreach ($recipients as $recipient) {
                if(filter_var($recipient, FILTER_VALIDATE_EMAIL)) {
                    $this->mailer->send('admin.email.job-application', $data, function ($m) use ($job, $recipient, $supervisor, $settings) {
                        $m->from(env('SENDING_DOMAIN'), $settings->site_name ?: 'Ayala Malls 360');
                        $m->to($recipient, $supervisor ?: 'Unknown')->subject("New Application for '{$job->title}'");
                    });
                }
            }
        } else {
            Log::alert("An email notification of job application for '{$job->title}' has failed to send. Cause: No recipient email registered!");
        }
    }
}
