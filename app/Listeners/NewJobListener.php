<?php

namespace App\Listeners;

use App\Events\NewJob;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Ayala\News\News;
use Ayala\Training\Training;
use Ayala\Learning\Learning;

class NewJobListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  NewJob  $event
     * @return void
     */
    public function handle(NewJob $event)
    {
        $job = $event->job;
        $employee = $event->employee;
        $settings = SiteSettings::getSettings();

        $news = News::inRandomOrder()->first();
        $learning = Learning::inRandomOrder()->first();
        $training = Training::inRandomOrder()->first();

        /*$jobImageURL = asset($job->img_thumb_path);
        $newsImageURL = asset($news->img_thumb_path);
        $learningImageURL = asset($learning->img_thumb_path);
        $trainingImageURL = asset($training->img_thumb_path);*/

        $jobImageURL = $job ? asset($job->img_thumb_path) : null;
        $newsImageURL = $news ? asset($news->img_thumb_path) : null;
        $learningImageURL = $learning ? asset($learning->img_thumb_path) : null;
        $trainingImageURL = $training ? asset($training->img_thumb_path) : null;

        $data = [
            'employee' => $employee,
            'news' => $news,
            'job' => $job,
            'learning' => $learning,
            'training' => $training,
            'jobImageURL' => $jobImageURL,
            'newsImageURL' => $newsImageURL,
            'learningImageURL' => $learningImageURL,
            'trainingImageURL' => $trainingImageURL,
        ];

        if(filter_var($employee->email, FILTER_VALIDATE_EMAIL)) {
            $this->mailer->send('admin.email.new-job', $data, function ($m) use ($job, $employee, $settings, $news, $learning, $training, $jobImageURL, $newsImageURL, $learningImageURL, $trainingImageURL) {
                $m->from(env('SENDING_DOMAIN'), 'Ayala Malls - Feedback');
                $m->to(env('APP_TESTING') ? env('APP_TESTING_EMAIL') : $employee->email, $employee->first_name)->subject($job->title);
            });
        }
    }
}
