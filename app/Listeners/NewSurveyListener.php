<?php

namespace App\Listeners;

use App\Events\NewSurvey;
use Ayala\SiteSettings\SiteSettings;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;

class NewSurveyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  NewSurvey  $event
     * @return void
     */
    public function handle(NewSurvey $event)
    {
        $survey = $event->survey;
        $employee = $event->employee;
        $settings = SiteSettings::getSettings();

        $surveyDescription = str_limit($survey->description, $limit = 200, $end = '...'); 
 
        $data = [
            'employee' => $employee,
            'survey' => $survey,
            'surveyDescription' => $surveyDescription
        ];

        if(filter_var($employee->email, FILTER_VALIDATE_EMAIL)) {
            $this->mailer->send('admin.email.new-survey', $data, function ($m) use ($survey, $employee, $settings, $surveyDescription) {
                $m->from(env('SENDING_DOMAIN'), 'Ayala Malls - Feedback');
                $m->to(env('APP_TESTING') ? env('APP_TESTING_EMAIL') : $employee->email, $employee->first_name)->subject($survey->title);
            });
        }
    }
}
