<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('auth/user', function (Request $request) {
    return $request->user();
});

// Get Auth
Route::group(['prefix' => 'auth'], function (){
    Route::get('user', 'AuthController@getAuth');
});

Route::post('employee/get', 'AuthController@getEmployee');

// Get Auth
Route::group(['prefix' => 'survey'], function () {
    Route::get('{survey}/get', 'SurveyController@get');
    Route::post('save', 'SurveyController@save');
    Route::post('submit', 'SurveyController@submit');
});
