<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Ayala\Feedback\Feedback;

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['namespace' => 'Frontend'], function (){

    // Guest Front View
    Route::group(['middleware' => 'guest'], function () {

        Route::get('/', 'HomeController@index')->name('home');
        Route::get('/feedback', 'HomeController@feedback')->name('feedback');
        Route::post('/feedback', 'HomeController@feedbackStore')->name('feedback.store');
        Route::get('/feedback/thank-you', 'HomeController@feedbackThankyou')->name('feedback.thank-you');

    });

    Route::group(['prefix' => 'account', 'namespace' => 'Account', 'middleware' => 'auth', 'as' => 'user.'], function () {

        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::get('mission-&-vision', 'DashboardController@missionVision')->name('mission-vision');
        Route::get('profile', 'AccountController@index')->name('profile');
        Route::post('profile', 'AccountController@update')->name('profile.update');

        Route::group(['prefix' => 'survey'], function () {
            Route::get('/', 'SurveyController@index')->name('survey');
            Route::get('{slug}/show', 'SurveyController@show')->name('survey.show');

            Route::get('{slug}/thank-you', 'SurveyController@thankYou')->name('survey.thank-you');
        });

        Route::group(['prefix' => 'training'], function (){
            Route::get('/', 'TrainingController@index')->name('training');
            Route::get('{slug}', 'TrainingController@show')->name('training.show');
            Route::post('{slug}/register', 'TrainingController@register')->name('training.register');
            Route::get('{slug}/register/cancel', 'TrainingController@cancel')->name('training.cancel');
            Route::get('{slug}/register/thank-you', 'TrainingController@thankYou')->name('training.thank-you');
        });

        Route::group(['prefix' => 'learning'], function () {
            Route::get('/', 'LearningController@index')->name('learning');
            Route::get('{slug}', 'LearningController@show')->name('learning.show');
        });

        Route::group(['prefix' => 'job'], function () {
            Route::get('/', 'JobController@index')->name('job');
            Route::get('department/{slug}', 'JobController@showDepartment')->name('job.department');
            Route::get('{slug}', 'JobController@show')->name('job.show');
            Route::post('{slug}/register', 'JobController@register')->name('job.register');
            Route::get('{slug}/register/thank-you', 'JobController@thankYou')->name('job.thank-you');
        });

        Route::group(['prefix' => 'news-&-events'], function () {
            Route::get('/', 'NewsController@index')->name('news');
            Route::get('{slug}', 'NewsController@show')->name('news.show');
        });
    });

});

/**
 * -----------------------------------------------------------
 * Admin Routes
 * -----------------------------------------------------------
 */

require base_path('routes/admin.php');