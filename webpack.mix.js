let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Copy Images Direcotry
mix.copyDirectory('resources/assets/sass/frontend/images', 'public/css/images');

// Front-end Styles & Scripts
mix.scripts([
    'resources/assets/js/vendor/jquery-2.2.4.js',
    'resources/assets/js/vendor/modernizr/modernizr-custom.js',
    'resources/assets/js/vendor/jValidate/jquery-validate.js',
    'resources/assets/js/vendor/jValidate/additional-methods.js',
    //'resources/assets/js/vendor/flexslider/jquery.flexslider.js',
    'resources/assets/js/vendor/swal2/sweetalert2.js',
    'resources/assets/css/widgets/jgrowl-notifications/jgrowl.js',
    //'resources/assets/js/vendor/bootstrap/tether.js',
    'resources/assets/js/vendor/bootstrap/bootstrap.js',
    'resources/assets/js/app.js'
], 'public/js/app.js');

mix.sass('resources/assets/sass/frontend/app.scss', 'public/css/app.css')
    .options({
        processCssUrls: false
    });

// Admin Styles && Scripts
mix.js('resources/assets/js/vue/app.js', './public/js/vue.js');
mix.scripts([
        'resources/assets/js/vendor/swal2/sweetalert2.js',
        'resources/assets/js/vendor/bootstrap/bootstrap-datepicker.js',
        'resources/assets/js/vendor/chosenImg/ImageSelect.jquery.js',
        'resources/assets/js/admin/app.js'
    ], 'public/js/admin.js');

mix.sass('resources/assets/sass/admin/app.scss', 'public/css/admin.css')
    .options({
        processCssUrls: false
    });

/*mix.styles([
    'resources/assets/css/helpers/animate.css',
    'resources/assets/css/helpers/boilerplate.css',
    'resources/assets/css/helpers/border-radius.css',
    'resources/assets/css/helpers/grid.css',
    'resources/assets/css/helpers/page-transitions.css',
    'resources/assets/css/helpers/spacing.css',
    'resources/assets/css/helpers/typography.css',
    'resources/assets/css/helpers/utils.css',
    'resources/assets/css/helpers/colors.css',
    'resources/assets/css/material/ripple.css',
    'resources/assets/css/elements/badges.css',
    'resources/assets/css/elements/buttons.css',
    'resources/assets/css/elements/content-box.css',
    'resources/assets/css/elements/dashboard-box.css',
    'resources/assets/css/elements/forms.css',
    'resources/assets/css/elements/images.css',
    'resources/assets/css/elements/info-box.css',
    'resources/assets/css/elements/invoice.css',
    'resources/assets/css/elements/loading-indicators.css',
    'resources/assets/css/elements/menus.css',
    'resources/assets/css/elements/panel-box.css',
    'resources/assets/css/elements/response-messages.css',
    'resources/assets/css/elements/responsive-tables.css',
    'resources/assets/css/elements/ribbon.css',
    'resources/assets/css/elements/social-box.css',
    'resources/assets/css/elements/tables.css',
    'resources/assets/css/elements/tile-box.css',
    'resources/assets/css/elements/timeline.css',
    'resources/assets/css/icons/fontawesome/fontawesome.css',
    'resources/assets/css/icons/linecons/linecons.css',
    'resources/assets/css/icons/spinnericon/spinnericon.css',
    'resources/assets/css/widgets/accordion-ui/accordion.css',
    'resources/assets/css/widgets/calendar/calendar.css',
    'resources/assets/css/widgets/carousel/carousel.css',
    'resources/assets/css/widgets/charts/justgage/justgage.css',
    'resources/assets/css/widgets/charts/morris/morris.css',
    'resources/assets/css/widgets/charts/piegage/piegage.css',
    'resources/assets/css/widgets/charts/xcharts/xcharts.css',
    'resources/assets/css/widgets/chosen/chosen.css',
    'resources/assets/css/widgets/colorpicker/colorpicker.css',
    'resources/assets/css/widgets/datatable/datatable.css',
    'resources/assets/css/widgets/datepicker/datepicker.css',
    'resources/assets/css/widgets/datepicker-ui/datepicker.css',
    'resources/assets/css/widgets/daterangepicker/daterangepicker.css',
    'resources/assets/css/widgets/dialog/dialog.css',
    'resources/assets/css/widgets/dropdown/dropdown.css',
    'resources/assets/css/widgets/file-input/fileinput.css',
    'resources/assets/css/widgets/input-switch/inputswitch.css',
    'resources/assets/css/widgets/input-switch/inputswitch-alt.css',
    'resources/assets/css/widgets/ionrangeslider/ionrangeslider.css',
    'resources/assets/css/widgets/jcrop/jcrop.css',
    'resources/assets/css/widgets/jgrowl-notifications/jgrowl.css',
    'resources/assets/css/widgets/loading-bar/loadingbar.css',
    'resources/assets/css/widgets/maps/vector-maps/vectormaps.css',
    'resources/assets/css/widgets/markdown/markdown.css',
    'resources/assets/css/widgets/modal/modal.css',
    'resources/assets/css/widgets/multi-select/multiselect.css',
    'resources/assets/css/widgets/multi-upload/fileupload.css',
    'resources/assets/css/widgets/nestable/nestable.css',
    'resources/assets/css/widgets/noty-notifications/noty.css',
    'resources/assets/css/widgets/popover/popover.css',
    'resources/assets/css/widgets/pretty-photo/prettyphoto.css',
    'resources/assets/css/widgets/progressbar/progressbar.css',
    'resources/assets/css/widgets/range-slider/rangeslider.css',
    'resources/assets/css/widgets/slidebars/slidebars.css',
    'resources/assets/css/widgets/slider-ui/slider.css',
    'resources/assets/css/widgets/summernote-wysiwyg/summernote-wysiwyg.css',
    'resources/assets/css/widgets/tabs-ui/tabs.css',
    'resources/assets/css/widgets/timepicker/timepicker.css',
    'resources/assets/css/widgets/tocify/tocify.css',
    'resources/assets/css/widgets/tooltip/tooltip.css',
    'resources/assets/css/widgets/touchspin/touchspin.css',
    'resources/assets/css/widgets/uniform/uniform.css',
    'resources/assets/css/widgets/wizard/wizard.css',
    'resources/assets/css/widgets/xeditable/xeditable.css',
    'resources/assets/css/snippets/chat.css',
    'resources/assets/css/snippets/files-box.css',
    'resources/assets/css/snippets/login-box.css',
    'resources/assets/css/snippets/notification-box.css',
    'resources/assets/css/snippets/progress-box.css',
    'resources/assets/css/snippets/todo.css',
    'resources/assets/css/snippets/user-profile.css',
    'resources/assets/css/snippets/mobile-navigation.css',
    'resources/assets/css/applications/mailbox.css',
    'resources/assets/css/themes/admin/layout.css',
    'resources/assets/css/themes/admin/color-schemes/default.css',
    'resources/assets/css/themes/components/default.css',
    'resources/assets/css/themes/components/border-radius.css',
    'resources/assets/css/helpers/responsive-elements.css',
    //'resources/assets/css/helpers/admin-responsive.css',
    'resources/assets/css/widgets/datatable/datatable.css',
    'resources/assets/css/widgets/chosen/chosen.css',
], 'public/css/delight.css')
.options({
    processCssUrls: false
});;

mix.scripts([
    'resources/assets/js/vendor/jquery-3.2.1.min.js',
    'resources/assets/css/js-core/jquery-core.js',
    'resources/assets/css/js-core/jquery-ui-core.js',
    'resources/assets/css/js-core/jquery-ui-widget.js',
    'resources/assets/css/js-core/jquery-ui-mouse.js',
    'resources/assets/css/js-core/jquery-ui-position.js',
    'resources/assets/css/js-core/transition.js',
    'resources/assets/css/js-core/modernizr.js',
    'resources/assets/css/js-core/jquery-cookie.js',
    'resources/assets/css/widgets/jgrowl-notifications/jgrowl.js',
    'resources/assets/css/widgets/spinner/spinner.js',
    'resources/assets/css/widgets/autocomplete/autocomplete.js',
    'resources/assets/css/widgets/autocomplete/menu.js',
    // 'delight/widgets/autocomplete/autocomplete-demo.js',
    'resources/assets/css/widgets/touchspin/touchspin.js',
    'resources/assets/css/widgets/touchspin/touchspin-demo.js',
    'resources/assets/css/widgets/input-switch/inputswitch.js',
    'resources/assets/css/widgets/input-switch/inputswitch-alt.js',
    'resources/assets/css/widgets/tabs-ui/tabs.js',
    'resources/assets/css/widgets/tabs/tabs.js',
    'resources/assets/css/widgets/tabs/tabs-responsive.js',
    'resources/assets/css/widgets/textarea/textarea.js',
    'resources/assets/css/widgets/multi-select/multiselect.js',
    'resources/assets/css/widgets/uniform/uniform.js',
    'resources/assets/css/widgets/uniform/uniform-demo.js',
    'resources/assets/css/widgets/chosen/chosen.js',
    // 'delight/widgets/chosen/chosen-demo.js',
    'resources/assets/css/widgets/dropdown/dropdown.js',
    'resources/assets/css/widgets/tooltip/tooltip.js',
    'resources/assets/css/widgets/popover/popover.js',
    'resources/assets/css/widgets/modal/modal.js',
    'resources/assets/css/widgets/dialog/dialog.js',
    // 'delight/widgets/dialog/dialog-demo.js',
    'resources/assets/css/widgets/progressbar/progressbar.js',
    'resources/assets/css/widgets/button/button.js',
    'resources/assets/css/widgets/collapse/collapse.js',
    'resources/assets/css/widgets/superclick/superclick.js',
    'resources/assets/css/widgets/input-switch/inputswitch-alt.js',
    'resources/assets/css/widgets/input-mask/inputmask.js',
    'resources/assets/css/widgets/slimscroll/slimscroll.js',
    'resources/assets/css/widgets/slidebars/slidebars.js',
    // 'delight/widgets/slidebars/slidebars-demo.js',
    'resources/assets/css/widgets/screenfull/screenfull.js',
    'resources/assets/css/widgets/content-box/contentbox.js',
    'resources/assets/css/widgets/material/material.js',
    'resources/assets/css/widgets/material/ripples.js',
    'resources/assets/css/widgets/overlay/overlay.js',
    // 'delight/js-init/widgets-init.js',
    'resources/assets/css/widgets/datepicker/datepicker.js',
    // 'delight/widgets/datepicker-ui/datepicker.js',
    // 'delight/widgets/datepicker-ui/datepicker-demo.js',
    'resources/assets/css/widgets/daterangepicker/moment.js',
    'resources/assets/css/widgets/daterangepicker/daterangepicker.js',
    'resources/assets/css/widgets/daterangepicker/daterangepicker-demo.js',
    'resources/assets/css/widgets/timepicker/timepicker.js',
    'resources/assets/css/widgets/noty-notifications/noty.js',
    'resources/assets/css/widgets/datatable/datatable.js',
    'resources/assets/css/widgets/datatable/datetime-moment.js',
    'resources/assets/css/widgets/datatable/datatable-bootstrap.js',
    // 'delight/widgets/datatable/datatable-tabletools.js',
    'resources/assets/css/themes/admin/layout.js',
    'resources/assets/css/widgets/chosen/chosen.js',
    'resources/assets/css/js-core/raphael.js',
    'resources/assets/css/widgets/charts/morris/morris.js',
    'resources/assets/css/widgets/charts/chart-js/chart-core.js',
    'resources/assets/css/widgets/charts/chart-js/chart-doughnut.js',
    'resources/assets/css/widgets/charts/chart-js/chart-bar.js',
    'resources/assets/css/widgets/wow/wow.js',
], 'public/js/delight.js');*/

/*
if (mix.inProduction()) {
    mix.version();
}*/
